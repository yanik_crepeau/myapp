#!/usr/bin/env coffee
require('./proj4.js')
mgrs = require('./mgrs.js')
fs = require('fs')

##Give the bounding box of your map using WGS84
##latitude and longitude coordinate.
left =  -74.0
right = -73.5
top = 	45.75
bottom = 45.5


latLngToBottomLeft = (latLng) ->
	mgrs1 = mgrs.forward(latLng,5)
	mgrs1_array = mgrs1.split("")
	zone = mgrs1_array[0..2].join("")
	bigSquare = mgrs1_array[3..4].join("")
	smallSquareE = mgrs1_array[5..9].join("")
	smallSquareN = mgrs1_array[10..14].join("")
	#console.log("#{latLng} -> #{zone} #{bigSquare} #{smallSquareE} #{smallSquareN}")
	console.log("#{zone} #{bigSquare} startWith")
	zone+bigSquare+"0000000000"

nextX = (mgrsStr) ->
	
	logMessage = "*"
	#split the current info, it needed later
	mgrs1_array = mgrsStr.split("")
	zoneD = mgrs1_array[0..2].join("")
	bigSquareD = mgrs1_array[3..4].join("")
	smallSquareED = mgrs1_array[5..9].join("")
	smallSquareND = mgrs1_array[10..14].join("")
	
	#get the current position in lat/long
	try 
		toPoint = mgrs.inverse(mgrsStr)
	catch e
		console.log("inverse1 #{mgrsStr} - #{e.message}")
	
	if toPoint[0] > right
		return null
	
	#Find the cosinus of the current latitude
	#I need to convert degrees into radians. (Math.PI/180.0)
	facteur = Math.cos(Math.PI * toPoint[1]/ 180.0)
	
	#How wide a a single degree of longitude at this latutude
	degre = 111320.0 * facteur
	nearest = Math.floor((toPoint[0] + 3.000) / 6.00) * 6
	partial = Math.floor(((degre * 6) % 100000) / 2)
	metre_EW = 1.0 / degre
	metre_NS = 1.0 / 111111.11
	moveNorth = 0.0
	moveEast = metre_EW * 1
	
	##At this point, I can find 3 different cases:
	##I am at more than 100km of the zone divide, so my 100km square are full
	##or, I am close to the the zone divide and I am east of it or west of it
	largeur =  (toPoint[0]-nearest) * degre

	if (Math.abs(largeur) > 100000)
		# I am in the case of a full 100km scenario
		tmpMgrs = 	"#{zoneD}#{bigSquareD}99999#{smallSquareND}"
		logMessage = "#{zoneD}#{bigSquareD} largeur: >100km - full square #{largeur.toFixed(0)} #{toPoint[0].toFixed(2)} - #{nearest} "
		
	else if (largeur < 0)
		largeur =  -largeur
		str_largeur = largeur.toFixed(0)
		while(str_largeur.length < 5)
			str_largeur = "0" + str_largeur
		moveNorth = metre_NS * 5
		moveEast = metre_EW * 100
		tmpMgrs = 	"#{zoneD}#{bigSquareD}#{str_largeur}#{smallSquareND}"
		logMessage = "#{zoneD}#{bigSquareD} west #{largeur.toFixed(0)} #{toPoint[0].toFixed(2)} - #{nearest} "
		
	else if (largeur < partial)
		largeur = partial-largeur+ parseInt(smallSquareED,10)
		str_largeur = largeur.toFixed(0)
		while(str_largeur.length < 5)
			str_largeur = "0" + str_largeur
		#moveNorth = metre_NS * 500
		moveEast = metre_EW *301
		tmpMgrs = 	"#{zoneD}#{bigSquareD}#{str_largeur}#{smallSquareND}"
		logMessage = "#{zoneD}#{bigSquareD} east1 #{largeur.toFixed(0)} #{toPoint[0].toFixed(2)} - #{nearest}  "
		
	else
		tmpMgrs = 	"#{zoneD}#{bigSquareD}99999#{smallSquareND}"
		logMessage = "#{zoneD}#{bigSquareD} east2 100km #{toPoint[0].toFixed(2)} - #{nearest} "
		
		
	
	try 
		toPoint = mgrs.inverse(tmpMgrs )
	catch erreur
		console.log("inverse2 #{tmpMgrs} - #{erreur.message}")
		console.log("logMessage: #{logMessage}")
	
		
	pointRight = [toPoint[2]+moveEast , toPoint[3]+moveNorth ]
	mgrs2 = mgrs.forward(pointRight , 5)
	mgrs2_array = mgrs2.split("")
	zoneA = mgrs2_array[0..2].join("")
	bigSquareA = mgrs2_array[3..4].join("")
	smallSquareEA = mgrs2_array[5..9].join("")
	smallSquareNA = mgrs2_array[10..14].join("")
		
		
	#console.log("#{mgrsStr} -> #{tmpMgrs} -> #{zoneA} #{bigSquareA} #{smallSquareEA} #{smallSquareNA}  #{logMessage }")
	#console.log("#{zoneA} #{bigSquareA} nextX")
	zoneA + bigSquareA + smallSquareEA + smallSquareNA 
		
nextY = (mgrsStr) ->
	logMessage = "*"
	metre_NS = 1.0 / 111111.11
	#split the current info, it needed later
	mgrs1_array = mgrsStr.split("")
	zoneD = mgrs1_array[0..2].join("")
	bigSquareD = mgrs1_array[3..4].join("")
	smallSquareED = mgrs1_array[5..9].join("")
	smallSquareND = mgrs1_array[10..14].join("")
	
	
	#get the current position in lat/long
	try 
		toPoint = mgrs.inverse(mgrsStr)
	catch e
		console.log("inverse1 #{mgrsStr} - #{e.message}")
	
	if toPoint[1] > top
		#console.log("nextY interrupted #{toPoint[1]} > #{top}")
		return null
	
	tmpStr = mgrsStr.substr(0,10)+"99999"	
	try 
		toPoint = mgrs.inverse(tmpStr)
	catch e
		console.log("inverse1 #{mgrsStr} - #{e.message}")
		
	pointTop = [toPoint[2] , toPoint[3]+metre_NS * 10.0 ]
	
	
	mgrs2 = mgrs.forward(pointTop , 5)
	mgrs2_array = mgrs2.split("")
	zoneA = mgrs2_array[0..2].join("")
	bigSquareA = mgrs2_array[3..4].join("")
	smallSquareEA = mgrs2_array[5..9].join("")
	smallSquareNA = mgrs2_array[10..14].join("")
		
		
	#console.log("#{mgrsStr} -> #{tmpStr } -> #{zoneA} #{bigSquareA} #{smallSquareEA} #{smallSquareNA}")
	#console.log("#{zoneA} #{bigSquareA} nextY")
	
	zoneA + bigSquareA + smallSquareEA + smallSquareNA 
	

bottomLeft = [left, bottom]
topRight = [right,top]

workload = []

origine = latLngToBottomLeft(bottomLeft)
for y in [0..0]
	leftSquare = origine
	for x in [0..1]
		##Take care of origine here
		workload.push origine.substr(0,5)
		origine = nextX(origine)
		if (origine == null)
			break
	origine = nextY(leftSquare)
	if (origine == null)
		break

console.log("Zones to cover: #{workload.length}")		

console.log("opening file for writing")

array_has_at_least_one_element = false

json = fs.createWriteStream('/Users/ycrepeau/Documents/Development/myapp/public/mgrs.json', {'flags': 'w'})
json.write("{ \n")
json.write('    "type":		"FeatureCollection",\n')
json.write('    "crs": {\n')
json.write('        "type": "name",\n')
json.write('        "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },\n')
json.write('    "features": [\n')

for job in workload
	console.log("Perform lines for #{job}")
	for x in [0..999]
		##Check if the line is more important than the other
		#zone TODO
		if (x == 0) then hectoKilo = "hectoKilo " else hectoKilo=""
		if ((x%100) == 0) then decaKilo ="decaKilo " else decaKilo=""
		if ((x%10) == 0) then kilo="kilo " else kilo=""
		##Other lines are every 100 meters.
		
		zoneId = job.substr(0,3)
		bigSquareId = job.substr(3,2)
		
		
		##Pad with 0 00 if needed to have 3-digit
		if (x < 10)
			x = "00#{x}"
		else if x < 100
			x = "0#{x}"
		pt1 = "#{job}#{x}0000000"
		pt2 = "#{job}#{x}0099999"
		mrs1 = mgrs.inverse(pt1)
		mrs2 = mgrs.inverse(pt2)
		
		if array_has_at_least_one_element 
			json.write(',')
		else
			array_has_at_least_one_element = true
		
		#console.log("V #{pt1} to #{pt2}")
		json.write('\n{\n')
		json.write('     "type":  "Feature",\n')
		json.write('     "properties":  { \n')
		json.write('          "id":  "' + pt1.substr(0,3) +
										"_" +
										pt1.substr(3,2) +
										"_" +
										"V#{x}" +
										'",\n')
		json.write('           "class": "'+ pt1.substr(0,3) +
										" " +
										pt1.substr(3,2) +
										" " +
										"V#{x} mgrsLine " + hectoKilo + decaKilo + kilo +
										'",\n')
		json.write('           "zoneId": "' + zoneId + '",\n') 
		json.write('           "bigSquareId": "' + bigSquareId  + '"\n') 
		json.write('     },\n')
		
		bottomH = mrs1[0]
		bottomV = mrs1[1]
		topH = mrs2[0]
		topV = mrs2[3]
		
		#console.log("bottomH: #{bottomH}, bottomV: #{bottomV}")
		#console.log("topH: #{topH}, topV: #{topV}")
		
		json.write('     "geometry":  {\n')
		json.write('           "type":  "LineString",\n')
		json.write('     "coordinates":  [\n')
		json.write("     [#{bottomH}, #{bottomV}], [#{topH}, #{topV}]")
		json.write('     ]\n')
		json.write('     }\n')
		json.write('  }')
		
	for y in [0..999]
		##Check if the line is more important than the other
		#zone TODO
		if (y == 0) then hectoKilo = "hectoKilo " else hectoKilo=""
		if ((y%100) == 0) then decaKilo ="decaKilo " else decaKilo=""
		if ((y%10) == 0) then kilo="kilo " else kilo=""
		##Other lines are every 100 meters.
		
		
		zoneId = job.substr(0,3)
		bigSquareId = job.substr(3,2)
		
		##Pad with 0 00 if needed to have 3-digit
		if (y < 10)
			y = "00#{y}"
		else if y < 100
			y = "0#{y}"
		pt1 = "#{job}00000#{y}00"
		pt2 = "#{job}99999#{y}00"
		mrs1 = mgrs.inverse(pt1)
		mrs2 = mgrs.inverse(pt2)
		
		if array_has_at_least_one_element 
			json.write(',')
		else
			array_has_at_least_one_element = true
		
		json.write('\n{\n')
		json.write('     "type":  "Feature",\n')
		json.write('     "properties":  { \n')
		json.write('          "id":  "' + pt1.substr(0,3) +
										"_" +
										pt1.substr(3,2) +
										"_" +
										"H#{y}" +
										'",\n')
		json.write('           "class": "'+ pt1.substr(0,3) +
										" " +
										pt1.substr(3,2) +
										" " +
										"H#{y} mgrsLine " + hectoKilo + decaKilo + kilo +
										'",\n') 
		json.write('           "zoneId": "' + zoneId + '",\n') 
		json.write('           "bigSquareId": "' + bigSquareId  + '"\n') 
		json.write('     },\n')
		
		bottomH = mrs1[0]
		bottomV = mrs1[1]
		topH = mrs2[2]
		topV = mrs2[1]
		
		#console.log("bottomH: #{bottomH}, bottomV: #{bottomV}")
		#console.log("topH: #{topH}, topV: #{topV}")
		
		json.write('     "geometry":  {\n')
		json.write('           "type":  "LineString",\n')
		json.write('     "coordinates":  [\n')
		json.write("     [#{bottomH}, #{bottomV}], [#{topH}, #{topV}]")
		json.write('     ]\n')
		json.write('     }\n')
		json.write('  }')
		

console.log("finishing the job and closing the file")
json.write('                ]\n')
json.write(' }\n')
json.end()

console.log("That's all folks!")

json.on('finish', () ->
	console.log("All data finished writing.")
	
	)




return
console.log("{ ")

console.log('	"type":		"FeatureCollection",')
console.log('	"features":	[{')
console.log('		"type":	"Feature",')
console.log('		"geometry": {')
console.log('				"type": "LineString",')
console.log('				"coordinates": [')
console.log('					[h1,v1], [h2,v2]')
console.log('				]},')
console.log('		"properties" : {')
console.log('			"id":	"anId",')
console.log('			"classe": "aClasse"')
console.log('		}')
console.log('	}]')

console.log("}")

###
{
"type": "FeatureCollection",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
                                                                                
"features": [
{ 
	"type": "Feature", 
	"properties": { 
		"CO_CEP": "579", 
		"NM_CEP": "Abitibi-Est", 
		"NM_TRI_CEP": "ABITIBIEST", 
		"ELEC_2012": 33261.000000 }, 
	"geometry": { 
		"type": "MultiPolygon", 
		"coordinates": [ 
			[ 
				[ 
					[ -75.520747945591793, 48.665465456621902 ], 
					[ -75.520754404747507, 48.660121643569781 ], 
					[ -75.520757283180814, 48.657439011141392 ], 
					[ -75.520759703076152, 48.655979522878447 ], 
					[ -75.520760381705045, 48.655222420304732 ], 
					[ -75.520761565723305, 48.654559793090421 ], 
					[ -75.520765853516508, 48.650962159145365 ], 
					[ -75.520766982384586, 48.649939790135122 ], 
					[ -75.520761590744883, 48.6487673901...


# MGRS lines vertical (NS)
for x in [977..1200] 
	if (x < 1000)
		pt1 = "18TWR#{x}0035400"
		pt2 = "18TWR#{x}0033250"
	else
		str_num = "a#{x}".substr(2,3)
		pt1 = "18TXR#{str_num}0035400"
		pt2 = "18TXR#{str_num}0033250"
		
	
	mrs1 = mgrs.toPoint(pt1)
	mrs2 = mgrs.toPoint(pt2)
	
	if (mrs2[0] != mrs1[0])
		pente = (mrs2[1] - mrs1[1]) / (mrs2[0] - mrs1[0])
		lngS = (bottom - mrs2[1]) / pente + mrs2[0]
		lngN = (top - mrs2[1]) / pente + mrs2[0]
		point1 = [top, lngN]
		point2 = [bottom, lngS]
		if (x % 10 != 0)
			classe = "mgrs vertical"
		else
			classe = "mgrs vertical strong"
			
		console.log("#{classe} M #{point1[1]} #{point1[0]} L #{point2[1]} #{point2[0]} ")


		
for y in [319..355]
	pt1 = "18TWR98000#{y}00"
	pt2 = "18TXR02000#{y}00"
	
	mrs1 = mgrs.toPoint(pt1)
	mrs2 = mgrs.toPoint(pt2)
	
	pente = (mrs2[1] - mrs1[1]) / (mrs2[0] - mrs1[0])
	
	left =  -73.7485
	right = -73.7150
	
	latE = mrs1[1] - pente * (left-mrs1[0])
	latW = mrs1[1] - pente * (right-mrs1[0])
	
	point1 = [latE, right]
	point2 = [latW, left]
	
	if (y % 10 != 0)
		classe = "mgrs horizontal"
	else
		classe = "mgrs horizontal strong"
		
	console.log("#{classe} M #{point1[1]} #{point1[0]} L #{point2[1]} #{point2[0]} ")
###	
	
	
