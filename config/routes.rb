Rails.application.routes.draw do
  resources :usagers

  get 'grille/index'

  get 'mgrs/index'

  get 'dorval/index'

  get 'levis/index'

  get 'carte_topo/index'

  get 'rails3/index'

  get 'carte2/index'

  get 'carte/index'

  get 'paris/index'

  get 'stm/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  root 'welcome#index'
  get 'paris/gencode' => 'paris#gencode'
  resources :paris
  get 'montreal/gencode' => 'montreal#gencode'
  resources :montreal
  get 'stm/gencode' => 'stm#gencode'
  resources :stm
  get 'carte/gencode' => 'carte#gencode'
  resources :carte
  get 'carte2/gencode' => 'carte2#gencode'
  resources :carte2
  get 'carte3/gencode' => 'carte3#gencode'
  resources :carte3
  get 'carte_topo/gencode' => 'carte_topo#gencode'
  resources :carte_topo
  get 'levis/gencode' => 'levis#gencode'
  resources :levis
  get 'smsj/gencode' => 'smsj#gencode'
  resources :smsj
  get 'dorval/gencode' => 'dorval#gencode'
  resources :dorval
  get 'grille/gencode' => 'grille#gencode'
  resources :grille
  
  
  get ':action' => 'static#:action'
  
end
