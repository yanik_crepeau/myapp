Leaflet.tile_layer = "http://{s}.tile.osm.org/{z}/{x}/{y}.png"
#Leaflet.subdomains = ['otile1', 'otile2', 'otile3', 'otile4']
Leaflet.attribution = "&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors"
Leaflet.max_zoom = 18