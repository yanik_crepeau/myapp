# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( stm.js)
Rails.application.config.assets.precompile += %w( montreal.js )
Rails.application.config.assets.precompile += %w( welcome.js )
Rails.application.config.assets.precompile += %w( paris.js )
Rails.application.config.assets.precompile += %w( carte.js )
Rails.application.config.assets.precompile += %w( carte2.js )
Rails.application.config.assets.precompile += %w( carte3.js )
Rails.application.config.assets.precompile += %w( carte_topo.js )
Rails.application.config.assets.precompile += %w( levis.js )
Rails.application.config.assets.precompile += %w( smsj.js )
Rails.application.config.assets.precompile += %w( dorval.js )
Rails.application.config.assets.precompile += %w( grille.js )

Rails.application.config.assets.precompile += %w( proj4.js )
Rails.application.config.assets.precompile += %w( mgrs.js )
Rails.application.config.assets.precompile += %w( usagers.js )



Rails.application.config.assets.precompile += %w( stm.css)
Rails.application.config.assets.precompile += %w( montreal.css )
Rails.application.config.assets.precompile += %w( welcome.css )
Rails.application.config.assets.precompile += %w( paris.css )
Rails.application.config.assets.precompile += %w( carte.css )
Rails.application.config.assets.precompile += %w( carte2.css )
Rails.application.config.assets.precompile += %w( carte3.css )
Rails.application.config.assets.precompile += %w( carte_topo.css )
Rails.application.config.assets.precompile += %w( levis.css )
Rails.application.config.assets.precompile += %w( smsj.css )
Rails.application.config.assets.precompile += %w( dorval.css )
Rails.application.config.assets.precompile += %w( grille.css )
Rails.application.config.assets.precompile += %w( usagers.css )


Rails.application.config.assets.precompile += %w( custom.css )

Rails.application.config.assets.precompile += %w( jquery-mobile/icons-png/action-white.png )
Rails.application.config.assets.precompile += %w( jquery-mobile/*.png )
Rails.application.config.assets.precompile += %w( jquery-mobile/ajax-loader.gif )

