# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup' if File.exist?(ENV['BUNDLE_GEMFILE'])

# Added by yjc 2017-08-27
if ENV['HEROKU_RELEASE_VERSION']
    $deploy_version = 'heroku ' + ENV['HEROKU_RELEASE_VERSION'] + ' ' + ENV['HEROKU_RELEASE_CREATED_AT']
else
    $deploy_version = 'local' 
end
