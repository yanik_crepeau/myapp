#!/usr/bin/ruby

ROOT_DIR = `git rev-parse --show-toplevel`.chomp

puts "Generating formatted source code for " + ROOT_DIR

def doProject(project)

	puts "Generating for project " + project
	destination_path        = "#{ROOT_DIR}/app/views/static/"
	
	haml_source_file        = "#{ROOT_DIR}/app/views/#{project}/index.html.haml "
	coffee_source_file      = "#{ROOT_DIR}/app/assets/javascripts/#{project}.js.coffee "
	sass_source_file        = "#{ROOT_DIR}/app/assets/stylesheets/#{project}.css.scss "
	rb_source_file			= "#{ROOT_DIR}/app/controllers/#{project}_controller.rb"

	haml_destination_file   = destination_path + "#{project}_haml.html.erb "
	coffee_destination_file = destination_path + "#{project}_coffee.html.erb "
	sass_destination_file   = destination_path + "#{project}_sass.html.erb "
	rb_destination_file		= destination_path + "#{project}_rb.html.erb "

	proc	= '/usr/local/bin/pygmentize -f html -O linenos=inline,encoding="utf-8",tabsize=4,full,style=colorful -o '


	begin
	    p1 = Process.spawn(proc + haml_destination_file 	+ haml_source_file,		:err=>:out)
	    p2 = Process.spawn(proc + coffee_destination_file 	+ coffee_source_file,	:err=>:out)
	    p3 = Process.spawn(proc + sass_destination_file 	+ sass_source_file,		:err=>:out)
	    p4 = Process.spawn(proc + rb_destination_file 		+ rb_source_file,		:err=>:out)
	
		Process.wait(p1)
		Process.wait(p2)
		Process.wait(p3)
		Process.wait(p4)
	
		puts "End process for #{ROOT_DIR} and #{project}"
	rescue Exception => e  
		puts "*** EXCEPTION raised during Process.spawn (#{project})"
	    puts e.message  
	    puts e.backtrace.inspect 
		puts "*** *** ***"
	end
end

doProject('stm')
doProject('montreal')
doProject('paris')
doProject('carte3')
doProject('carte2')
doProject('carte')
doProject('carte_topo')




puts "Committing"
#exec("cd #{destination_path}; git commit -a -m 'auto-commit'")


