class CreateUsagers < ActiveRecord::Migration
  def change
    create_table :usagers do |t|
      t.string :nom
      t.string :courriel
      t.string :passe

      t.timestamps
    end
  end
end
