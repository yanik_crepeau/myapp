module StaticHelper

	def coffee
		 link_to "coffee", controller: "static", action:  action_name.split('_')[0] + "_coffee"
	end
	
	def sass
		 link_to "sass", controller: "static", action:  action_name.split('_')[0] + "_sass"
	end
	
	def haml
		 link_to "haml", controller: "static", action:  action_name.split('_')[0] + "_haml" 
	end
	
	def rb
		 link_to "controller ruby", controller: "static", action:  action_name.split('_')[0] + "_rb"
	end
	
	def retour
		link_to action_name.split('_')[0], controller:  action_name.split('_')[0]
	end
end

