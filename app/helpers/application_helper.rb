module ApplicationHelper

	def gencode_helper
		haml_tag :div, :id =>"sourceCodeBox" do
			haml_tag :h3 , "Watch source code file"
			if Rails.env.development?
				haml_tag :p do
					haml_concat "Please call"
					haml_tag :a, "#{controller_name}/gencode", :href=>"#{controller_name}/gencode"
					haml_concat "to force the server to (re-)generate fully formated source"
					haml_concat "code available for public view. The code is automatically"
					haml_concat "called each time 'git commit' is called."
				end
			else
				haml_tag :p do
					haml_concat "The gencode script is run automatically just before"
					haml_concat "deploying the app on heroku. When the server is running"
					haml_concat "in development mode, this paragraph is replaced with another"
					haml_concat "one that makes a manual call to /stm/gencode possible."
				end
			end
			
			haml_tag :ul do
				haml_tag :li do
					haml_tag :a, "Coffescript (client side executable) code", :href=>"/#{controller_name}_coffee"
				end
				haml_tag :li do
					haml_tag :a, "Sass (format) code", :href=>"/#{controller_name}_sass"
				end
				haml_tag :li do
					haml_tag :a, "This page in haml format.", :href=>"/#{controller_name}_haml"
				end
				haml_tag :li do
					haml_tag :a, "Ruby controller (server side executable) code", :href=>"/#{controller_name}_rb"
				end
				
			end
			
			haml_tag :p do
				haml_concat "The htmlized versions of the code above is generate by"
				haml_tag :a, "http://pygments.org", :href=>'http://pygments.org'
			end
		end
	end


end
