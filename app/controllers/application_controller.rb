class ApplicationController < ActionController::Base
	# Prevent CSRF  attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception

	## The purpos it the 'gencode' method is to launch subprocesses aimes
	#  to create htmlized versions of the source code.
	def gencode
		puts "gencode starts #{controller_name}" 
		if Rails.env.production?
			flash[:notice] = "gencode is not available on the production server (Heroku)"
			redirect_to  action: "index"
		end 

		destination_path        = "#{Rails.root}/app/views/static/"
		haml_source_file        = "#{Rails.root}/app/views/#{controller_name}/index.html.haml "
		coffee_source_file      = "#{Rails.root}/app/assets/javascripts/#{controller_name}.js.coffee "
		sass_source_file        = "#{Rails.root}/app/assets/stylesheets/#{controller_name}.css.scss "
		rb_source_file			= "#{Rails.root}/app/controllers/#{controller_name}_controller.rb"
	
		haml_destination_file   = destination_path + "#{controller_name}_haml.html.erb "
		coffee_destination_file = destination_path + "#{controller_name}_coffee.html.erb "
		sass_destination_file   = destination_path + "#{controller_name}_sass.html.erb "
		rb_destination_file		= destination_path + "#{controller_name}_rb.html.erb "
	
		proc	= '/usr/local/bin/pygmentize -f html -O linenos=inline,encoding="utf-8",tabsize=4,full,style=colorful -o '
		flash[:notice] = "launching 4 processes. destinatino path: #{destination_path} \n"
		begin
			p1 = Process.spawn(proc + haml_destination_file 	+ haml_source_file,		:err=>:out)
			Process.detach(p1)
			flash[:notice] += "haml: #{haml_source_file} \n"
			puts "#{haml_source_file} --> #{haml_destination_file}"
			
			p2 = Process.spawn(proc + coffee_destination_file 	+ coffee_source_file,	:err=>:out)
			Process.detach(p2)
			flash[:notice] += "coffee: #{coffee_source_file} \n"
			puts "#{coffee_source_file} --> #{coffee_destination_file}"
			
			p3 = Process.spawn(proc + sass_destination_file 	+ sass_source_file,		:err=>:out)
			Process.detach(p3)
			flash[:notice] += "sass: #{sass_source_file} \n"
			puts "#{sass_source_file} --> #{sass_destination_file}"
			
			p4 = Process.spawn(proc + rb_destination_file 		+ rb_source_file,		:err=>:out)
			Process.detach(p4)
			flash[:notice] += "rb: #{rb_source_file}"
			puts "#{rb_source_file} --> #{rb_destination_file}"
			
		rescue Exception => e  
			puts "*** EXCEPTION raised during Process.spawn "
			puts e.message  
			puts e.backtrace.inspect 
			puts "*** *** ***"
			flash[:alert] = "Process.spawn EXCEPTION: " + e.message
		end
	
		
		redirect_to  action: "index"
	end

			

end

