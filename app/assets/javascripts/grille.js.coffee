# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->

    $('body').addClass('grille')

    voisines = null
    smsj = null
    feature = null
    noms = null
    countries = null

    ts = () ->
        moment().format("ss.SSS")

    projectPoint = (x, y) ->
        point = map.latLngToLayerPoint(new L.LatLng(y, x))
        this.stream.point(point.x, point.y)


    console.log("#{ts()} Projet carte topo")

    stamenAttribution = 'Tuiles cartographiques: '+
    '<a href="http://stamen.com">Stamen Design</a>,'+
    ' sous licence '+
    '<a href="http://creativecommons.org/licenses/by/3.0/deed.fr">'+
    'CC BY 3.0</a>. Données: '+
    '<a href="http://openstreetmap.org">OpenStreetMap</a>, '+
    'sous licence <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
    map = new L.Map("map", {
        center: [45.518, -73.547],          # Centre de la carte (lat,lon)
        zoom: 13,                           # Niveau de zoom au démarage
        minZoom: 5                          # Niveau de zoom minimum
        maxZoom: 18                         # Niveau de zoom maxumum
        maxBounds: [[43.0,-82], [51, -60]]  # Zone affichable
        attributionControl: true
    })

    layer = new L.StamenTileLayer("toner-lite")

    map.addLayer(layer)

    #i18n... set attribution in French.
    a1 = map.attributionControl.getContainer().innerHTML.split("|")
    a2 = a1[1]
    map.attributionControl.getContainer().innerHTML = a1[0] + " | " +
        stamenAttribution

    #create the overlay d3 layer
    svg = d3.select(map.getPanes().overlayPane).append("svg")
    g = svg.append("g").attr("class", "leaflet-zoom-hide")

    #Setup the transform mecanism
    transform = d3.geo.transform({point: projectPoint})
    path = d3.geo.path().projection(transform)

    #This is the funny part, add some static text
    left = g.append('text')
        .attr("class", "left leaflet-zoom-hide")
        .text("MGRS — USNG — MGRS — USNG —")
        .attr("text-anchor", "left")
        .attr("transform", "")
        .attr("dy", "45px")

    right = g.append('g').attr("class", "right leaflet-zoom-hide")

    #DragStart and DragEnd events are called when the user drags the
    #map without changing the zoome. I subscribe to the dragEnd event
    #to (re)set everything in position.
    dragEnd = () ->
        console.log("DragEnd starts")

        #Drawing elements depens on the zoom level
        zoomLevel = map.getZoom()

        #Get the map bounds
        mapBounds = map.getBounds()

        #Get the corners and the center
        #in Map (LatLng) coordinates
        southWest = mapBounds.getSouthWest()
        southEast = mapBounds.getSouthEast()
        northEast = mapBounds.getNorthEast()
        northWest = mapBounds.getNorthWest()
        south      = mapBounds.getSouth()
        west      = mapBounds.getWest()
        east      = mapBounds.getEast()
        north      = mapBounds.getNorth()
        center = mapBounds.getCenter()

        #Get the corners and the center in
        #the layer (pixel) coordinates
        center_layer     = map.latLngToLayerPoint(center)
        southWest_layer = map.latLngToLayerPoint(southWest)
        southEast_layer = map.latLngToLayerPoint(southEast)
        northWest_layer = map.latLngToLayerPoint(northWest)
        northEast_layer = map.latLngToLayerPoint(northEast)

        #Get the corners and the cent in
        #MGRS format
        southEast_mgrs = mgrs.forward([southEast.lng, southEast.lat], 5)
        southWest_mgrs = mgrs.forward([southWest.lng, southWest.lat], 5)
        northWest_mgrs = mgrs.forward([northWest.lng, northWest.lat], 5)
        northEast_mgrs = mgrs.forward([northWest.lng, northWest.lat], 5)


        #Get the latitude at the center of the map
        latidude_center = center.lat

        #One meter expressed in degrees
        #this is not exact at the submilimeter level but
        #that will do the job.
        metre_inDegreeNS = 1.0/ 111111.11
        metre_inDegreeEW = parseFloat(1.0 / (111111.11 * Math.cos(latidude_center * Math.PI / 180.0)))
        #console.log("metres_inDegreeEW: #{metre_inDegreeEW}")


        #Move the left message to its position... at left
        left.attr("transform", "translate(#{southWest_layer.x}, #{southWest_layer.y}) rotate(-90)")


        #Remove any existing right side labels
        $(".right").empty()

        ##Utility functions

        #Split a mgrs string into components, separated with a space.
        human = (mgrsStr) ->

            pattern = ///
                ^(.{3})(.{2})(\d{5})(\d{5})
            ///
            match = mgrsStr.match(pattern)
            if (match == null)
                return mgrsStr + " !!"
            pattern2 = ///
                ^(\d0?[1-9]*)0*
            ///
            matchEasting = match[3].match(pattern2)
            matchNorthing = match[4].match(pattern2)
            return match[1] + " " +
                match[2] + " " +
                matchEasting[1] + " " +
                matchNorthing[1]
        #

        #Sets the southwest corner with most
        #other elements at 0,0
        mgrs_floor = (mgrs) ->
            if mgrs.length != 15 then return null
            if zoomLevel < 9 then return null
            if zoomLevel < 12
                positionV = 10
                positionH = 5
                complement = "00000"
            else if zoomLevel < 15
                positionV = 10
                positionH = 6
                complement = "0000"
            else
                positionV = 11
                positionH = 7
                complement = "000"
            ret = null
            if (complement.length < 5)
                ret = mgrs.substr(0,positionH) + complement +
                    mgrs.substr(10,5-complement.length) + complement
            else
                ret = mgrs.substr(0,positionH) + complement  + complement
            ret


        #

        # Find the square just North of the
        # square passed in parameter
        # Note: Dimension of the square is set
        # according the zoom level.
        mgrs_NW = (mgrsStr) ->
            if mgrsStr.length != 15 then return null
            if zoomLevel < 9 then return null

            #get the south west point in Long/Lat
            toPoint = mgrs.inverse(mgrsStr)
            #if we have left the map, return null
            if (toPoint[1] > north) then return null

            if zoomLevel < 12
                positionV = 9
                positionH = 5
                complement = "99999"
            else if zoomLevel < 15
                positionV = 10
                positionH = 6
                complement = "9999"
            else
                positionV = 11
                positionH = 7
                complement = "999"

            tmp_mgrs = mgrsStr.substr(0,positionV+1) + complement

            try
                toPoint = mgrs.inverse(tmp_mgrs)


            catch error
                console.log("#{tmp_mgrs} (#{tmp_mgrs.length} - original: #{mgrsStr})")
                console.log(error)
                return null

            toPoint[1] = parseFloat(toPoint[1]) + metre_inDegreeNS
            try
                rep_mgrs = mgrs.forward(toPoint,5)

            catch error
                return null

            rep_mgrs

        #

        # Find the square just East of the
        # square passed in parameter. Once again
        # the method is very zoom level dependant
        # to determine the size of the square.
        mgrs_SE = (mgrsStr) ->
            if mgrsStr.length != 15 then return null
            if zoomLevel < 9 then return null
            toPoint = mgrs.inverse(mgrsStr)

            ##Special case, if we are close to a separation zone
            ##(first 2 digits of a mgrs)
            toPoint = mgrs.inverse(mgrsStr)
            latWest = toPoint[0]
            #if we are outside the map, stop!
            if latWest > east
                return null
            closestLimit = Math.floor((latWest+3.0) / 6.0  )*6
            distance = (latWest - closestLimit) / metre_inDegreeEW;
            #console.log("latWest: #{latWest} closestLimit #{closestLimit} diff: #{latWest - closestLimit} distance: #{distance}")
            if zoomLevel < 12
                positionV = 9
                positionH = 5
                complement = "99999"
                if (distance > -100000 && distance < 0)
                    #console.log("distance: #{distance}")
                    rightPoint = [closestLimit+metre_inDegreeEW , toPoint[1]]
                    return mgrs.forward(rightPoint)

            else if zoomLevel < 15
                positionV = 10
                positionH = 6
                complement = "9999"
                if (distance > -10000 && distance < 0)
                    rightPoint = [closestLimit+metre_inDegreeEW , toPoint[1]]
                    return mgrs.forward(rightPoint)

            else
                positionV = 11
                positionH = 7
                complement = "999"
                if (distance > -1000 && distance < 0)
                    rightPoint = [closestLimit+metre_inDegreeEW , toPoint[1]]
                    return mgrs.forward(rightPoint)

            tmp_mgrs = mgrsStr.substr(0,positionH) + complement + mgrsStr.substr(10)
            try
                toPoint = mgrs.inverse(tmp_mgrs)

            catch error
                console.log("#{tmp_mgrs} (#{tmp_mgrs.length} - original: #{mgrsStr})")
                console.log(error)
                return null

            toPoint[0] = parseFloat(toPoint[0]) + metre_inDegreeEW
            try
                rep_mgrs = mgrs.forward(toPoint,5)

            catch error
                return null

            rep_mgrs


        #

        #
        # Draw the horizontal lines, add the marker
        # on the right side of the map.
        borderPointsH = (mgrsStr, vShift=0) ->
            if zoomLevel < 9
                return null
            if zoomLevel < 12
                positionH = 5
                positionV = 10
            if zoomLevel < 15
                positionH = 6
                positionV = 11
            else
                positionH = 7
                positionV = 12


            deltaDigit = parseInt(mgrsStr[positionH],10)
            if deltaDigit < 5
                deltaDigit = 9
            else
                deltaDigit = 0
            deltaMgrs = mgrsStr
            deltaMgrs = mgrsStr.substr(0, positionH) + deltaDigit + mgrsStr.substr(positionH + 1)
            deltaMgrs = deltaMgrs.substr(0,positionV) + vShift + deltaMgrs.substr(positionV + 1)
            mgrsStr   = mgrsStr.substr(0,positionV) + vShift + mgrsStr.substr(positionV + 1)

            column = human(mgrsStr).split(" ")[2]
            row    = human(mgrsStr).split(" ")[3]

            if (row.length == 1 || row == '00')
                classes  = " decaKilo kilo"
            else if (row.length == 2) then classes = " kilo"
            else if (row.length >= 3) then classes = " "

            pt1 = mgrs.inverse(mgrsStr)
            pt2 = mgrs.inverse(deltaMgrs)

            slope = (pt1[1]-pt2[1]) / (pt1[0]-pt2[0])
            east_y = slope * (east - pt1[0]) + pt1[1]
            west_y = slope * (west - pt1[0]) + pt1[1]
            pt_east = map.latLngToLayerPoint([east_y, east])
            pt_west = map.latLngToLayerPoint([west_y, west])
            right.append('line')
                .attr("id", "V_" +  mgrsStr)
                .attr("class", "mgrsLine " + classes)
                .attr("x1", "#{pt_west.x}")
                .attr("y1", "#{pt_west.y}")
                .attr("x2", "#{pt_east.x}")
                .attr("y2", "#{pt_east.y}")

            right.append('text')
                .text(row)
                .attr("transform", "translate(#{pt_east.x}, #{pt_east.y})")
                .attr("class", "hMarker" + classes)
                .attr("dx", "-10px")
                .attr("dy", "-10px")
                .attr("text-anchor", "end")


        #
        borderPointsV = (mgrsStr, hShift=0) ->
            if zoomLevel < 9
                return null
            if zoomLevel < 12
                positionH = 5
                positionV = 10
            if zoomLevel < 15
                positionH = 6
                positionV = 11
            else
                positionH = 7
                positionV = 12

            deltaDigit = parseInt(mgrsStr[positionV],10)
            if deltaDigit < 5
                deltaDigit = 9
            else
                deltaDigit = 0

            deltaMgrs = mgrsStr
            deltaMgrs = mgrsStr.substr(0, positionV) + deltaDigit + mgrsStr.substr(positionV + 1)
            deltaMgrs = deltaMgrs.substr(0,positionH) + hShift + deltaMgrs.substr(positionH + 1)
            mgrsStr   = mgrsStr.substr(0,positionH) + hShift + mgrsStr.substr(positionH + 1)
            #console.log(" V >> #{hShift} : #{human(mgrsStr)} to #{human(deltaMgrs)} #{mgrsStr} #{deltaMgrs}")

            column = human(mgrsStr).split(" ")[2]
            row    = human(mgrsStr).split(" ")[3]
            squareId = null
            squareIdW = null
            #console.log("#{mgrsStr} #{column} #{row}")

            if (column.length == 1 || column == '00')
                classes  = " decaKilo kilo"


            else if (column.length == 2) then classes = " kilo"
            else if (column.length >= 3) then classes = " "

            pt1 = mgrs.inverse(mgrsStr)
            pt2 = mgrs.inverse(deltaMgrs)

            if pt1[0] == pt2[0]
                #line north south - vertical line
                #return [[pt1[0], south], [pt2[0], north]]
                pt_south = map.latLngToLayerPoint([south, pt1[0]])
                pt_north = map.latLngToLayerPoint([north, pt2[0]])

            else
                slope = (pt1[1]-pt2[1]) / (pt1[0]-pt2[0])
                south_x = pt1[0] +  (south - pt1[1]) / slope
                north_x = pt1[0] +  (north - pt1[1]) / slope
                #return [[south_x, south], [north_x, north]]
                pt_south = map.latLngToLayerPoint([south, south_x])
                pt_north = map.latLngToLayerPoint([north, north_x])

            right.append('line')
                .attr("id", "V_" +  mgrsStr)
                .attr("class", "mgrsLine " + classes)
                .attr("x1", "#{pt_south.x}")
                .attr("y1", "#{pt_south.y}")
                .attr("x2", "#{pt_north.x}")
                .attr("y2", "#{pt_north.y}")
            right.append('text')
                .text(column)
                .attr("transform", "translate(#{pt_north.x}, #{pt_north.y}) rotate(90)")
                .attr("class", "vMarker" + classes)

            if (column == '00' && row == '00')
                squareId = human(mgrsStr).split(" ")[1]
                pt_line = map.latLngToLayerPoint([pt1[1], pt1[0]])
                #Find the other square ids TODO
                squareIdW = human(mgrs.forward([pt1[0]-100*metre_inDegreeEW , pt1[1]+100*metre_inDegreeNS])).split(" ")[1]
                squareIdSW = human(mgrs.forward([pt1[0]-100*metre_inDegreeEW , pt1[1]-100*metre_inDegreeNS])).split(" ")[1]
                squareIdS = human(mgrs.forward([pt1[0]+100*metre_inDegreeEW, pt1[1]-100*metre_inDegreeNS])).split(" ")[1]
                #Find the y position

                console.log("Adding #{squareId} at #{pt1[1].toFixed(3)} #{pt1[0].toFixed(3)} W:#{squareIdW} SW: #{squareIdSW} S:#{squareIdS} — #{mgrsStr}")
                right.append('text')
                    .text(squareId)
                    .attr("transform", "translate(#{pt_line.x}, #{pt_line.y})")
                    .attr("class", "vMarker" + classes)
                    .attr("dx", "5px")
                    .attr("dy", "-15px")
                    .attr("text-anchor", "start")
                right.append('text')
                    .text(squareIdW)
                    .attr("text-anchor", "end")
                    .attr("transform", "translate(#{pt_line.x}, #{pt_line.y})")
                    .attr("class", "vMarker" + classes)
                    .attr("dx", "-5px")
                    .attr("dy", "-15px")
                right.append('text')
                    .text(squareIdS)
                    .attr("transform", "translate(#{pt_line.x}, #{pt_line.y})")
                    .attr("class", "vMarker" + classes)
                    .attr("dx", "5px")
                    .attr("dy", "15px")
                    .attr("text-anchor", "start")
                right.append('text')
                    .text(squareIdSW)
                    .attr("transform", "translate(#{pt_line.x}, #{pt_line.y})")
                    .attr("class", "vMarker" + classes)
                    .attr("dx", "-5px")
                    .attr("dy", "15px")
                    .attr("text-anchor", "end")


        ## Test code here
        if zoomLevel < 9
            console.log("No grid draw for zoomLevel (#{zoomLevel}) < 9")

        #I start somewhere outside the map (at southwest)
        pt = mgrs_floor(southWest_mgrs)

        if pt != null
            console.log("start: #{pt} (#{pt.length}) at ZoomLevel: #{zoomLevel}")
            ptx = pt
            for i in [0..5]
                pty = ptx
                for j in [0..5]
                    #console.log("  #{i}-#{j}: #{human(pty)} (#{pty.length})")
                    for x in [0..9]
                        borderPointsV(pty,x)
                        borderPointsH(pty,x)
                    pty = mgrs_NW(pty)
                    if (pty == null)
                        break

                ptx = mgrs_SE(ptx)
                if (ptx == null)
                    break



    reset = () ->
        ##Reset the corners of the svg


        bounds = path.bounds(countries)
        topLeft = bounds[0]
        bottomRight = bounds[1]
        svg.attr("width", bottomRight[0] - topLeft[0])
            .attr("height", bottomRight[1] - topLeft[1])
            .style("left", topLeft[0] + "px")
            .style("top", topLeft[1] + "px")
        g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")")



        #Le svg prend classe selon le zoom: z9, z10, z11 .. z18
        #Voir smsj.css.scss pour voir l'effet (aka certains éléments invisibles)
        svg.attr("class", "z#{map.getZoom()}")

        if feature != null
            feature.attr("d", path)
        if (noms != null)
            noms
                .attr("transform", (d) ->
                    #Je trouve le centroid de la circonscription
                    #(lon,lat) et je transforme ce point dans le
                    #système de coordonnées du moment (qui varie selon
                    #le zoom)
                    centroid = map.latLngToLayerPoint(d.latLng)
                    "translate(#{centroid.x}, #{centroid.y})")
                .attr("visibility", (d) ->
                    #Je vérifie la surface (area) de la circonscription
                    #selon le zoom du moment. Si la valeur passe en dessous
                    #d'un certaine valeur, ja cache le nom de la circonscription
                    rep = "novalue"
                    if path.area(d)<6000.0
                        rep = "hidden"
                    else
                        #Ici je n'indiue pas "visible" parce que
                        #toute la couche est masquée pendant certaines
                        #operations. En utilisant 'inherit', si la couche
                        #est visible, le nom de la circonscription le sera
                        #aussi mais si la couche n'est pas visible, le
                        #nom de la circonscription en fera de même
                        rep = "inherit"

                    #console.log("#{d.properties.name} area: #{path.area(d)}  ––  #{rep}")
                    rep)

                .attr("district_area", (d) ->
                    path.area(d))

                .style("font-size", (d) ->
                    #ici aussi j'utilise le surface de la circonscription
                    #dans le contexte du zoom pour déterminer la taille des
                    #caractères
                    rep = "3pt"
                    if path.area(d) >= 25000.0
                        rep = "18pt"
                    else if path.area(d)>6500.0
                        rep = "12pt"
                    else
                        rep = "6pt"

                    rep)


        ## Redessiner différents éléments avec le nouveau système de référence
        ## déterminé en fonction du zoom.

        #left.attr("transform", "translate(#{bounds[0][0]})")
        #Getting the current map view bounds
        dragEnd()






    d3.json "/ca.json", (error, data) ->
        if (error)
            console.log("#{ts()} error loading data #{error}")
            alert("Probème de réception des fichiers de contours")
            return
        else
            console.log("#{ts()} ca.json (sections de vote - tracé des limites) reçu. ")

            countries = topojson.feature(data, data.objects.countries)
            ###
            #Les circonscriptions voisines de Sainte-Marie=Saint-Jacques
            voisines = topojson.feature(data, data.objects.electionsQC3)

            #Les sections de vote dans SMSJ
            smsj = topojson.feature(data, data.objects.smsj)


            # J'ajoute les limites des circonscriptions
            feature = g.selectAll(".district")
                .data(voisines.features)
                .enter()
                .append("path")
                    .attr("id", (d) ->
                        d.id)                    #le id de la circonscription n'a aucun préfixe
                    .attr("class", (d) ->
                        "district d" + d.id)
                    .attr("name", (d) ->
                        d.properties.name + " QC")

            noms = g.selectAll("text.nom")
                        .data(voisines.features)
                        .enter()
                        .append("text")
                            .attr("id", (d) ->
                                "n" + d.id)        #je précède le numéro de circonscription de "n"
                                                #afin qu'il n'y ait pas deux object avec le même id.
                            .text((d) ->
                                d.properties.name)
                            .attr("district_area", "nil")
                            .attr("class", "nom")
                            .attr("text-anchor", "middle")
                            .attr("transform", (d) ->
                                centroid = path.centroid(d)                        #je trouve le centroid de la circonscription
                                d.latLng = map.layerPointToLatLng(centroid )    #que je traduit dans le système de référence Leaflet
                                "translate(#{centroid[0]}, #{centroid[1]})")    #et je l'enregistre avec 'd'. Et je déplace le nom au bon endroit
            ###



            map.on("dragend", dragEnd)
            map.on("viewreset", reset);
            reset();





    console.log("#{ts()} Map creation completed, adding D3 layer")
