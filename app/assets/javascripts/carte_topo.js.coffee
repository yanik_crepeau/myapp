# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#  

$ ->
	ts = () ->
		moment().format("ss.SSS")

	console.log("#{ts()} Projet carte topo")
	$('body').addClass('carte_topo')
	
	uniqueId = "A8506231-215E-406A-B12D-EEEE97D7573A"
	uniqueId2 = "A8506231-215E-406A-B12D-EEEE97D7573A_resultats"
	
	utmProjectionData = '+proj=utm +zone=18 +ellps=GRS80 +units=m +no_defs'
	utm = null
	utm_res = null
	
	console.log("#{ts()} utmProjectionData: #{utmProjectionData }")
	
	try
		utm = proj4(utmProjectionData) 
		utm_res = proj4(utm).forward([-73.551111, 45.522222])
		console.log("#{ts()} UTM: #{utm_res}")
		
		dep = -360.0
		pointSWutm = [579064.81681,  5005567.0-dep]
		pointNEutm = [682075.92803,  5070213.0-dep]
		
		pointSW = proj4(utm).inverse(pointSWutm)
		pointNE = proj4(utm).inverse(pointNEutm)
		
		console.log("#{ts()} SW: #{pointSW[0].toFixed(3)} #{pointSW[1].toFixed(3)}")
		console.log("#{ts()} NE: #{pointNE[0].toFixed(3)} #{pointNE[1].toFixed(3)}")
		
	catch e
		console.error("proj4 init returned error: #{e.message}")
	
	try
		mr = mgrs.forward([-73.551111111, 45.522222222],3)
		console.log("#{ts()} #{mr[0..2]} #{mr[3..4]} #{mr[5..7]} #{mr[8..10]}")
		$('#position .top .zone').text("#{mr[0..2]} #{mr[3..4]}")
		$('#position .bottom').text("#{mr[5..7]} #{mr[8..10]}")
	catch e
		console.error("#{ts()} Error while getting mgrs conversion: #{e.message}")
		
	image_url = "/31H_250K.jpeg"
	imageBounds = L.latLngBounds([[pointSW[1],pointSW[0]], [pointNE[1],pointNE[0]]])
	
	map = new L.Map("map", {
			center: [45.53, -73.5], 
			zoom: 10,
			minZoom:10,
			maxZoom:10,
			zoomControl:false})
				#.addLayer(new L.TileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png",
				#			{	attribution: "&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors " +
				#			"| &copy <a href=\"http://www.electionsquebec.qc.ca\">Le Directeur général des élections du Québec"}))
	#.fitBounds(imageBounds)
	try
		overlay = L.imageOverlay(image_url , imageBounds)
		    .addTo(map)
	catch e
		console.error("#{ts()} EXCEPTION while adding image to map: #{e.message}")
		
	map.dragging.disable();
	map.touchZoom.disable();
	map.doubleClickZoom.disable();
	map.scrollWheelZoom.disable();
	map.boxZoom.disable();
	map.keyboard.disable();
		
	console.log("#{ts()} Map creation completed, adding D3 layer")
	
	try
		#create the overlay d3 layer
		svg = d3.select(map.getPanes().overlayPane).append("svg")
		g = svg.append("g").attr("class", "leaflet-zoom-hide")
	catch e
		console.error("#{ts()} EXCEPTION while creating svg/g: #{e.message}")
		
	console.log("#{ts()} d3 layer created. About to load data")

	drawConstituenciesHasCompleted = false
	#utility functions
	projectPoint = (x, y) ->
		point = map.latLngToLayerPoint(new L.LatLng(y, x))
		this.stream.point(point.x, point.y)

	
	
	#Get the overlay data used to draw constituencies lines

	drawConstituencies = (data) ->	
		console.log("#{ts()} data loaded, building the districts")
		
		##We work only with this portion of the file.
		geojsondata = topojson.feature(data, data.objects.electionsQC)
		
		## The reset() function redraw the map and update the path of every
		## district area. It is called when zooming in and out.
		reset = () ->
			##Reset the corners of the svg 
			bounds = path.bounds(geojsondata)
			topLeft = bounds[0]
			bottomRight = bounds[1]
			svg.attr("width", bottomRight[0] - topLeft[0])
				.attr("height", bottomRight[1] - topLeft[1])
				.style("left", topLeft[0] + "px")
				.style("top", topLeft[1] + "px")
			g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")")
			
			## Redraw the distrist borders using a new zoomed projection
			feature.attr("d", path)
			
			if (cercles != null)
				cercles
					.attr("transform", (d) ->
						centroid = map.latLngToLayerPoint(d.latLng)
						"translate(#{centroid.x}, #{centroid.y})")
					.attr("visibility", (d) ->
						rep = "novalue"
						if path.area(d)<500.0 
							rep = "hidden"
						else
							rep = "inherit"
							
						#console.log("#{d.properties.name} area: #{path.area(d)}  ––  #{rep}")
						rep)
					.attr("district_area", (d) ->
						path.area(d))
		
		
		
		#setup the transformation mecanism
		transform = d3.geo.transform({point: projectPoint})
		path = d3.geo.path().projection(transform)
		
		# Add the district to the map
		feature = g.selectAll(".district")
			.data(geojsondata.features)
			.enter()
			.append("path")
				.attr("id", (d) ->
					d.id)
				.attr("class", (d) ->
					"district d" + d.id)
				.attr("name", (d) ->
					d.properties.name + " QC")
					
		# And the dot
		console.log("#{ts()} pinning with 2nd...")
		cercles = g.selectAll("circle.pin")
			.data(geojsondata.features)
			.enter()
			.append("circle")
				.attr("id", (d) ->
					"c" + d.id)
				.attr("name", (d) ->
					d.properties.name)
				.attr("district_area", "nil")
				.attr("class", "pin")
				.attr("transform", (d) ->
					centroid = path.centroid(d)
					d.latLng = map.layerPointToLatLng(centroid )
					"translate(#{centroid[0]}, #{centroid[1]})")
				.attr("r", "5")
		
				
		#make sure when the map is zoomed, the reset is called, and
		#call reset anayway to complete the district drawing process		
		map.on("viewreset", reset);
		reset();
		

		console.log("#{ts()} Constituencies drawing completed!")
		drawConstituenciesHasCompleted = true



	
	displayResults = (elections2014) ->
		
		if (!drawConstituenciesHasCompleted)
			setTimeout ( ->
				displayResults(elections2014)
			), 2500
			console.log("#{ts()} posponing result display... base map not ready")
			return
		
		circonscriptions = elections2014.circonscriptions
		console.log("#{ts()} circonscriptions loaded (#{circonscriptions.length})")
		for circonscription, i in circonscriptions 
			numero = circonscription.numeroCirconscription
			candidats = circonscription.candidats
			partiElu = candidats[0].numeroPartiPolitique
			partiSecond = candidats[1].numeroPartiPolitique
			classeParti = ""
			
			if partiElu > 0
				switch partiElu
					when 6 then classeParti = "plq"
					when 8 then classeParti = "pq"
					when 11 then classeParti = "qs"
					when 27 then classeParti = "caq"
					else classeParti = "autreParti"
				repere = "path\##{numero}"
				#console.log("Assigning #{classeParti} to #{repere} - #{circonscription.nomCirconscription}")
				try 
					$(repere)[0]
						.setAttribute("class", "district d#{numero} elu #{classeParti}")
				catch e
					console.error("EXCEPTION jQuery parti gagnant #{repere}")

			if partiSecond > 0
				switch partiSecond 
					when 6 then classeParti = "plq"
					when 8 then classeParti = "pq"
					when 10 then classeParti = "vert"
					when 11 then classeParti = "qs"
					when 26 then classeParti = "on"
					when 27 then classeParti = "caq"
					else classeParti = "autreParti"
				repere = "circle\#c#{numero}"
				#console.log("Assigning pin #{classeParti} to #{repere} - #{circonscription.nomCirconscription}")
				try
					$(repere)[0]
						.setAttribute("class", "pin d#{numero} second #{classeParti}")
				catch e
					console.error("EXCEPTION jQuery parti 2nd #{repere}")
		
		console.log("#{ts()} all items coloured accordint results")
	

		

	
	

	#start here
	
	console.log("#{ts()} starting")
	d3.json "/ca.json", (error, data) ->
		if (error)
			console.log("#{ts()} error loading data #{error}")
		else
			#save(data)
			drawConstituencies(data)
				
	
	#Load the last election result data
	#and set the area color (using CSS)
	console.log("#{ts()} loading results from server ")
	d3.json "/resultats.json", (error, elections2014) ->
		if error 
			console.log("Unable to load /resultats.json #{error}")
			return
		else
			console.log("#{ts()} /resultats.json finished loading results")
			#saveResults(elections2014)
			
			displayResults(elections2014)
				
	console.log("#{ts()} that's all folks!")
				

	
	
	
	
