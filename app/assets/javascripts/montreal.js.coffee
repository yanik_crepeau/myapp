# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
	console.log('montreal.js loaded')
	
	arrName ='#plateau'
	#btngroup handler
	$('#radio').buttonset()
		
	$("input").change (event)->
		console.log('change  ' + event.target.id)
		arrName = event.target.id
				
	#called by the slider each time its value changes
	changeSize = (name, newSize) ->
		
		ns = "http://www.w3.org/2000/svg"
		
		g = $(name)[0]
		if g.nodeName == "path"
			parent = g.parentNode
			parent.removeChild(g)
			g2 = document.createElementNS(ns, 'g');
			g2.insertBefore(g,null)
			parent.insertBefore(g2,null)
			tempId = g.getAttribute('id')
			g.setAttribute('id', null)
			g2.setAttribute('id', tempId)
			g = g2	
			console.log("path inserted in an enclosing g element " + g2.getAttribute('id'))						
		rect = g.getBBox()
		w = rect.width;
		h = rect.height;
		x = rect.x;
		y = rect.y;
		
		##Recolor
		$(name).attr('style', 'fill: blue')
		$(name + " path").attr('style', 'fill: blue')
		
		#create a colored semi-transparent background
		# Check if the first node is not already an enclosing rect
		rect = g.firstChild
		if rect.nodeName == 'rect'
			if newSize <= 1
				g.removeChild(rect)
				$(name + ' path').attr('style', 'stroke:0')
			if newSize > 1
				$(name + ' path').attr('style', 'stroke-width:3px; stroke:white')
		else
			if newSize > 1
				$(name + ' path').attr('style', 'stroke-width:3px; stroke:white')
				rect = document.createElementNS(ns, 'rect');
				rect.setAttribute('x', x);
				rect.setAttribute('y', y);
				rect.setAttribute('width', w);
				rect.setAttribute('height', h);
				rect.setAttribute('style', "fill:rgba(64,64,64,0.9);stroke-width:3;stroke:rgb(0,0,0)")
				g.insertBefore(rect, g.firstChild)
		#make sure our g element is above others
		parent = g.parentNode
		parent.removeChild(g)
		parent.insertBefore(g,null)
		
		#resize and re-center
	
		s = newSize
		center = x + w/2
		middle = y + h/2
		inv = 1 - s
		g.setAttribute('transform', "translate(\#{center * inv}, \#{middle * inv}) scale(\#{s})")



	## setup the slider
	$('#slider').slider({min: 1, max: 5,step: 0.25,slide: (event, ui) ->
													changeSize(arrName, ui.value)
						})
	## load Montreal map and make it available for jquery
	main_chart_svg = document.getElementById('anotherTest')
	
	d3.xml "/mtl.svg", (error, documentFragment) ->
		if (error)
			console.log(error)
			return
			
		svgNode = documentFragment.getElementsByTagName("svg")[0]
		
		main_chart_svg.appendChild(svgNode)
		
		$('#layer1').attr('transform', 'scale(0.5) translate(320,0)')

		arr = ['#outremont', 
				'#ilebizarre',
				'#pierrefond',
				'#ahuntsic',
				'#CoteDesNeigesNotreDameDeGrace', 
				'#saintlaurent', 
				'#VilleraySaintMichelParcExtension',
				'#villemarie', 
				'#plateau', 
				'#stmichel',
				'#rosemont', 
				'#sudouest', 
				'#LaSalle',
				'#Lachine',
				'#Verdun', 
				'#MercierHochelagaMaisonneuve',
				'#PointeAuxTrembles',
				'#anjou',
				'#MontrealNord']
		for arrondissement, i in arr
			$(arrondissement).attr('style', "fill:#000066;")
			$(arrondissement + " path").attr('style', "fill:#112244;")
		
