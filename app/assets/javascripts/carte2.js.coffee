# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
	console.log('carte2.js.coffee loading')
	
	width = 1920
	height = 1080
	
	svg = null		# rend la variable svg globale
	path = null		# voir arrierePlan
	
	arrierePlan = (data) ->
		#On efface tout
		$('#carte_container').empty()
		
		#Ajout d'un élément svg
		try
			svg = d3.select("#carte_container").append("svg")
				.attr("width", width)
				.attr("height", height)
		catch e
			msg "ERROR: ajout svg\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
			
		try
			subunits 	= topojson.feature(data, data.objects.subunits)
		catch e
			console.log "ERROR:37\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
		try
			facteur = 12.0
			projection 	= d3.geo.mercator()
				.scale(1000 * facteur)
				.translate([width * 0.71 * facteur, height* 0.9 * facteur])
				.clipExtent([[0,0],[width*0.75,height]])
		catch e
			console.log "ERROR:46\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
		
		#Create a path element according to the projection
		try
			path = d3.geo.path()
				.projection(projection)
		catch e
			console.log "ERROR:55\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
			
		#read the subunits elements in the json document
		#and create the back map.
		try
			svg.append("path")
				.datum(subunits)
				.attr("class", "back")
				.attr("d", path)
		catch e
			console.log "ERROR:67\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
	pays = (data) ->
		#Add the country shapes
		try
			svg.selectAll(".country")
				.data(topojson.feature(data, data.objects.countries).features)
				.enter()
				.append("path")
					.attr("class", (d) ->
						"country " + d.id)
				    .attr("d", path)
		catch e
			console.log "ERROR:pays\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
	provinces = (data) ->
		#Add the country shapes
		try
			svg.selectAll(".provinces")
				.data(topojson.feature(data, data.objects.provinces).features)
				.enter()
				.append("path")
					.attr("class", (d) ->
						"province " + d.id)
					.attr("name", (d) ->
						d.properties.name + " " + d.properties.pays)
				    .attr("d", path)
		catch e
			console.log "ERROR:pays\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
		otherCountries = ["USA", "MEX", "CUB", "BLZ", "BHS"]

		#Setup boundries
		try
			svg.append("path")
			    .datum(topojson.mesh(data, data.objects.provinces, (a, b) -> 
					a != b && a.properties.pays not in otherCountries))
			    .attr("d", path)
			    .attr("class", "province-boundary");
		catch e
			console.log "ERROR:93\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return


		#but for the USA, just draw an outline
		try
			svg.append("path")
			    .datum(topojson.mesh(data, data.objects.provinces, (a, b) ->
					a != b && a.properties.pays in otherCountries))
			    .attr("d", path)
			    .attr("class", "province-boundary USA");
		catch e
			console.log "ERROR:104\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
				
		

	frontieresNationales = (data) ->
		otherCountries = ["USA", "MEX", "CUB", "BLZ", "BHS"]
		
		#but for the USA, draw an outline
		try
			svg.append("path")
			    .datum(topojson.mesh(data, data.objects.countries, (a, b) ->
					a.id in otherCountries))
			    .attr("d", path)
			    .attr("class", "outline USA");
		catch e
			console.log "ERROR:104\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
		try
			svg.append("path")
			    .datum(topojson.mesh(data, data.objects.countries, (a, b) ->
					a.id == 'CAN'))
			    .attr("d", path)
			    .attr("class", "outline CAN");
		catch e
			console.log "ERROR:104\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
		
	lacs = (data) ->
		#Add the country shapes
		try
			svg.selectAll(".lac")
				.data(topojson.feature(data, data.objects.lakes).features)
				.enter()
				.append("path")
					.attr("class", (d) ->
						"lac")
				    .attr("d", path)
		catch e
			console.log "ERROR:pays\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
	electionsCA = (data) ->
		try
			svg.selectAll(".district")
				.data(topojson.feature(data, data.objects.electionsCA).features)
				.enter()
				.append("path")
					.attr("class", (d) ->
						"district d" + d.id)
					.attr("name", (d) ->
						d.properties.name + " " + d.properties.codeprov)
				    .attr("d", path)
		catch e
			console.log "ERROR:123\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return
		
	msg = (message) ->
		# Fonction utile qui affiche un message dans
		# la console et ajoute le même message sur la
		# page.
		console.log message
		$('#carte_container').append message
	
	


	# Appel de démarage. 
	# C'est la première chose que le script fait quand
	# il est chargé. Son rôle est de charger le fichier 
	# ca.json qui contient les données décrivant les
	# différents polygones.
	$.get '/ca.json'
		.error (jqXHR, textStatus, errorThrown) -> 
			msg "AJAX Error: ${textStatus}."
		.success (data) -> 
			arrierePlan (data)
			pays(data)
			provinces(data)
			frontieresNationales(data)
			lacs(data)
			electionsCA(data)
