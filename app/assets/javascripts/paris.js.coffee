# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ -> 
	
	# Matrix functions
	deltaTransformPoint = (matrix, point) ->
		dx = point.x * matrix.a + point.y * matrix.c + 0
		dy = point.x * matrix.b + point.y * matrix.d + 0
		{ x: dx, y: dy }

	decomposeMatrix  = (matrix) ->

		# @see https://gist.github.com/2052247

		# calculate delta transform point
		px = deltaTransformPoint(matrix, { x: 0, y: 1 })
		py = deltaTransformPoint(matrix, { x: 1, y: 0 })

		# calculate skew
		skewX = ((180 / Math.PI) * Math.atan2(px.y, px.x) - 90)
		skewY = ((180 / Math.PI) * Math.atan2(py.y, py.x))

		{
			translateX: matrix.e,
			translateY: matrix.f,
			scaleX: Math.sqrt(matrix.a * matrix.a + matrix.b * matrix.b),
			scaleY: Math.sqrt(matrix.c * matrix.c + matrix.d * matrix.d),
			skewX: skewX,
			skewY: skewY,
			rotation: skewX # rotation is the same as skew x
		}		 
    
	showObject = (objetName) ->
		
		#console.log("showObject")
		myObject = $(objetName)[0]
		if myObject == undefined
			console.log("ERROR: " + objetName + " is not found!")
			return;
		
		matrix = myObject.getCTM()
		resultat = decomposeMatrix(matrix)

		#console.log("tranlateX: " + resultat.translateX + " translateY: " + resultat.translateY + " scaleX: " + resultat.scaleX + " scaleY: "+ resultat.scaleY)
		#console.log(objetName + " attr: "+ $(objetName).attr('transform'))
	   
		m = myObject.getCTM();
		$(myObject).attr("transform", "matrix("+m.a+","+m.b+","+m.c+","+ m.d+","+m.e+","+m.f+")")
	


	
	# Declare object id viewport as panzoom object
	$('#viewport').panzoom();
	
	#usefull global constants
	$panzoom = $('#viewport').panzoom()
	$parent  = $panzoom.parent()

	# setup the mousewheel to support zoom
	$parent.on 'mousewheel.focal', (e) ->
	    e.preventDefault();
	    delta = e.delta || e.originalEvent.wheelDelta;
	    zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
	    $panzoom.panzoom 'zoom', zoomOut, {
	      increment: 0.1,
	      animate: false,
	      focal: e
	    }
	

	#Setup button handlers
	$('#general').click ->
		$('#myImage').attr('xlink:href', '/Carte.svg')
		$('#viewport').attr('transform', "scale(0.60) translate(-250, -10)");
		showObject('#viewport');

	$('#cdg').click ->
		$('#myImage').attr('xlink:href', '/Carte.svg');
		$('#viewport').attr('transform', "scale(1.5) translate(-1000, -15)");
		showObject('#viewport');

	$('#Paris_centre').click ->
		$('#myImage').attr('xlink:href', '/Carte.svg');
		$('#viewport').attr('transform', "scale(1.5) translate(-550, -300)");
		showObject('#viewport');

	$('#Paris_rivegauche').click ->
		$('#myImage').attr('xlink:href', '/Carte.svg');
		$('#viewport').attr('transform', "scale(3) translate(-600, -500)");
		showObject('#viewport');


	$('#metro_general').click ->
		$('#myImage').attr('xlink:href', '/plan.svg');
		$('#viewport').attr('transform', "scale(1.05) translate(-380, -300)");
		showObject('#viewport');
	$('#metro_riveGauche').click ->
		$('#myImage').attr('xlink:href', '/plan.svg');
		$('#viewport').attr('transform', "scale(1.75) translate(-470, -500)");
		showObject('#viewport');
	$('#metro_segment1').click ->
		$('#myImage').attr('xlink:href', '/plan.svg');
		$('#viewport').attr('transform', "scale(3.5) translate(-650, -600)");
		showObject('#viewport');


	$('#metro_riveDroite_Ouest').click ->
		$('#myImage').attr('xlink:href', '/plan.svg');
		$('#viewport').attr('transform', "scale(1.75) translate(-400, -300)");
		showObject('#viewport');
		
	$('#metro_riveDroite_Boulevards').click ->
		$('#myImage').attr('xlink:href', '/plan.svg');
		$('#viewport').attr('transform', "scale(2.50) translate(-550, -385)");
		showObject('#viewport');

	$('#metro_riveDroite_Iles').click ->
		$('#myImage').attr('xlink:href', '/plan.svg');
		$('#viewport').attr('transform', "scale(4.50) translate(-700, -500)");
		showObject('#viewport');
		
	$('#metro_riveDroite_Est').click ->
		$('#myImage').attr('xlink:href', '/plan.svg');
		$('#viewport').attr('transform', "scale(1.80) translate(-650, -350)");
		showObject('#viewport');


	$('#metro_M6_0').click ->
		$('#myImage').attr('xlink:href', '/Metro_Paris_M6-plan.svg');
		$('#viewport').attr('transform', "scale(0.5) translate(-0, -350)");
		showObject('#viewport');

	$('#metro_M6_1').click ->
		$('#myImage').attr('xlink:href', '/Metro_Paris_M6-plan.svg');
		$('#viewport').attr('transform', "scale(2.5) translate(-750, -470)");
		showObject('#viewport');

	$('body').addClass('paris_js_loaded')
	console.log('paris.js loaded')
	

		
