#
# Place all the behaviors and hooks related to the matching controller here.
# You can use CoffeeScript in this file: http://coffeescript.org/
# Ok to deploy version 1.0.0
#
$ ->
	console.log("Sainte-Marie — Saint-Jacques ...")
	$('body').addClass('smsjjs_loaded')

	## 	Liste des candidatEs
	#	--------------------

	###
		La variable 'liste_candidats' doit être adaptée par
		circonscription. C'est un 'Array' et chaque
		entrée (une par candidatE) comprend trois éléments:

		clef: 	La clef utilisée par la DGEQ pour identifier
				la colonne dans le fichier csv contenant els
				résultats
		nom:	Le nom du ou de la candidatE
		parti:	Le parti de ce candidat ou de cette candidate
	###
	liste_candidats = [
		{
			clef: "Bissonnette Marc B.P."
			nom: "Marc Bissonnette"
			parti:"Bloc Pot"
			abreviation: 'bp'
		},
		{
			clef: "Breton Daniel P.Q."
			nom: "Daniel Breton"
			parti:"Parti Québécois"
			abreviation: 'pq'

		},
		{
			clef: "Klisko Anna P.L.Q./Q.L.P."
			nom: "Anna Klisko"
			parti: "Parti Libéral du Québec"
			abreviation: 'plq'

		},
		{
			clef: "Lachapelle Serge P.M.L.Q."
			nom: "Serge Lachapelle"
			parti:"Parti Marxiste-Léniniste"
			abreviation: 'pmlq'

		},
		{
			clef: "Massé Manon Q.S."
			nom: "Manon Massé"
			parti: "Québec solidaire"
			abreviation: 'qs'
		},
		{
			clef: "Payne Nic O.N. - P.I.Q."
			nom: "Nic Payne"
			parti: "Option Nationale, Parti Indépendantiste du Québec"
			abreviation: 'onPiq'

		},
		{
			clef: "Thauvette Patrick C.A.Q.-É.F.L."
			nom: "Patrick Thauvette"
			parti: "Coallition Avenir-Québec / Équipe François Legault"
			abreviation: 'caq'

		},
		{
			clef: "Wiseman Stewart P.V.Q./G.P.Q."
			nom: "Stewart Wiseman"
			parti: "Parti Vert du Québec"
			abreviation: 'pvq'

		}
	]

	#Carte de la circonscription en la divisant
	#par section de vote (en date du 2014-04-07)
	#Source: DGEQ http://www.electionsquebec.qc.ca/documents/zip/sections%20de-vote-elections-2014-shapefile.zip

	###
	Note:	Ce fichier zip comprend un répertoire de plusieurs
			fichiers et le fichier *.shp à lui
			seul occupe pas moins de 44Mo. En utilisant les
			outils GDAL (ogr2org) je transforme cet
			ensemble de fichiers en un fichier GeoJSON puis
			topoJSON en faisant l'extraction des données
			concernant uniquemet la circonscription de Lévis
			(code 663 selon le DGEQ)

			Comme le fichier est encodé en utilisant le vieux
			format ISO-8859-1 (Latin1), il est important
			de le réencoder en utilisant le standard Unicode (UTF8).
			Pour ce faire, il faut indiquer que
			la source (Sections\ de\ Vote\ Elections\ 2014_04_07.shp)
			est encodée ISO-8859-1:

			export SHAPE_ENCODING="ISO-8859-1"

			Ensuite on fait tourner l'utilitaire ogr2ogr pour
			extraire les données, les réencoder en format
			UTF8 et sauvegarder le tout en format GeoJSON

			ESRI: 4326 http://spatialreference.org/ref/epsg/wgs-84/
			ogr2ogr: http://www.gdal.org/ogr2ogr.html

			ogr2ogr -f GeoJSON -where "CO_CEP = '663'" \
				-t_srs EPSG:4326 smsj.json \
				Sections\ de\ Vote\ Elections\ 2014_04_07.shp \
				-lco ENCODING=UTF-8

			Enfin, la commande: ogrinfo -al -geom=summary smsj.json | less
			permet de jeter un coup d'oeil au fichier créé est constater:

			1-	Que les caractères accentuées on été ré-encodés en
				UTF-8 est sont lisibles (Lévis)
			2-	Les références utilisent des longitudes et latitudes
				(le -t_srs EPSG:4326 en est reponsable)
			3-	Seuls les données de Lévis sont dans ce
				fichier (-where "CO_CEP='663'")

			N'oubliez pas de mettre l'option -geom=summary (ou -geom=no)
			si ne voulez pas être submergéE par les données géométriques
			(qui sont très abondantes mais pas vraiment lisibles par
			un humain).

			Le fichier smsj.json pèse maintenant xxxKo (au lieu les 46Mo
			à l'origine), mais il y a moyen de faire mieux. N'oublions pas
			que ce fichier va transiter sur Internet et son temps de
			transmission s'ajoutera au temps que prendra Javascript
			pour dessiner la carte.

			J'aimerais cependant avoir les limites des circonscriptions
			voisines. Mon but est d'avoir les limites des différentes
			circonscriptions qui entourent Lévis et à l'intérieur de Lévis
			avoir les limites de chaque section de vote.

			L'ensemble des limites des circonscriptions est disponibles
			sur le site du DGEQ ici:

			http://www.electionsquebec.qc.ca/documents/zip/circonscriptions-electorales-2011-shapefile-v2.zip

			Encore une fois, il convient de convertir les
			données qui à l'origine utilisent l'encodage
			ISO-8859-1 (Latin1) vers l'Unicode (UTF-8). De plus,
			je n'ai pas besoin des limites de toutes
			les circonscriptions du Québec, seulement les
			circonscriptions voisines de Sainte-Marie — Saint-Jacques.

			ogr2ogr	-f GeoJSON  \
					-t_srs EPSG:4326 \
					-clipdst -72.172 46.34 -70.172 47.34 \
					-overwrite \
					../electionsQC2.json carte.shp \
					-lco ENCODING=UTF-8

			Note: Cette commande produit une erreur (exception)
			et est incapable de rendre la circonscription de
			Chute-de-la-Chaudière. Pour pallier à ce problème
			j'ai remplacé le "-clipdst ..." par "-where CO_CEP in (...)", le
			... étant la liste des circonscriptions qui doivent être
			incluses dans le fichier final. 24 au total.



			Et nous terminons avec topojson pour combiner à la fois les
			limites des circonscriptions de la région et les limites
			des sections de vote dans Sainte-Marie-Saint-Jacques:

			topojson 	-o ~/Documents/Development/myapp/public/smsj.json  \
						-q 1e6 \
						--id-property CO_CEP,NO_SV \
						--properties name=NM_CEP \
						--shapefile-encoding utf8 \
						--  electionsQC2.json Levis.json

			où
			-o				nom du fichier de destination
			-q				quand tourner les coins ronds
			--id-property	Quel élément servira de id
			--properties	Les propriétés à conserver
							(NM_CEP deviendra 'name')
			--				fichiers devant être combinés
	###

	##	Variables globales
	#	------------------

	svg = null				#l'élément svg qui servira de contenant
	path = null				#contiendra l'object servant à assurer
							# une bonne projection
	map	= null				#l'objet qui contiendra la carte
	feature = null			#les tracés (surface) des circonscriptions
	bfeature = null			#les tracés (contour-lignes) de circonscriptions (meme parti)
	cfeature = null			#les tracés (contour-lignes) de circonscriptions (plusieurs parties)
	sections = null			#les tracés des sections
	numSecs = null			#les noms des circonscriptons
	resultats = []			#Globale servant de minibase de données

	##	Fonctions utilitaires
	#	---------------------

	#Utilisé pour transformer les coordonnés d'un point
	#de Lat/Long au système de coordonné local
	projectPoint = (x, y) ->
		point = map.latLngToLayerPoint(new L.LatLng(y, x))
		this.stream.point(point.x, point.y)


	#Sert à imprimer les secondes et millisecondes (timestamp)
	#pour vérifier les performances d'exécution (pour du débug)
	ts = () ->
		moment().format("ss.SSS")



	#Ajoute des pattern (hachure) pour illustrer les partis s'étant
	#classé premier et deuxième
	addPattern = (name, backgroundColor, challengeColor, wider = false) ->

		pat = svg.append("pattern")
			.attr("id", name)
			.attr("patternUnits", "userSpaceOnUse")
			.attr("width", "20")
			.attr("height", "20")



		pat.append('rect')
				.attr("width", "20")
				.attr("height", "20")
				.attr("fill", backgroundColor)

		if wider
			# Note: les lignes ne suivent pas la même orientation
			# Il y a 3 lignes, plus fines mais qui au total occupent plus d'espace
			pat.append("path")
					.attr("d", "M20,0 L0,20 M10,0 L0,10 M20,10 L 10,20")
					.attr("stroke", challengeColor)
					.attr("stroke-width", "0.5")
					.attr("stroke-linecap", "butt")
		else
			pat.append("path")
					.attr("d", "M0,0 l20,20")
					.attr("stroke", challengeColor)
					.attr("stroke-width", "1.5")
					.attr("stroke-linecap", "butt")



	setUpPatterns = () ->
		##Créer les patterns (SVG)
		sec_trans = 0.3
		plq_c = "rgba(255,0,0,0.4)"

		addPattern("qsChallengeCAQ", "rgba(132,112,255,#{sec_trans} )", "rgba(255,102,0,0.5)")
		addPattern("qsChallengePLQ", plq_c , "rgba(255,192,40,1.0)", true)
		addPattern("qsChallengePQ", "rgba(0,0,128,#{sec_trans})", "rgba(255,102,0,0.5)")

		addPattern("pqChallengeCAQ", "rgba(132,112,255,#{sec_trans})", "rgba(0,0,128,0.5)")
		addPattern("pqChallengePLQ", plq_c , "rgba(0,0,128,0.5)")
		addPattern("pqChallengeQS", "rgba(255,102,0,#{sec_trans} )", "rgba(0,0,128,0.5)")

		addPattern("caqChallengePQ", "rgba(0,0,128,#{sec_trans})", "rgba(132,112,255,0.5)")
		addPattern("caqChallengePLQ", plq_c , "rgba(132,112,255,0.5)")
		addPattern("caqChallengeQS", "rgba(255,102,0,#{sec_trans})", "rgba(132,112,255,0.5)")

		addPattern("plqChallengeCAQ", "rgba(132,112,255,#{sec_trans})", plq_c )
		addPattern("plqChallengePQ", "rgba(0,0,128,#{sec_trans})", plq_c )
		addPattern("plqChallengeQS", "rgba(255,102,0,#{sec_trans})", plq_c )






	#Lorsque la fenêtre de dialogue donnant les données complètes pour
	#une section donnée s'ouvre, il faut tout préparer.
	setUpDialog = (d) ->
		qs  = 0
		plq = 0
		pq = 0
		caq = 0
		autres = 0
		total_angle = 0.0

		html_table = "<table class='resultats'>"
		html_table += "<tr><th class='cand'>CandidatE</th><th class='vote'>Votes</th><th id='chart' rowspan=9> </th></tr>"

		for c,i in resultats[d.id].lcandidats
			html_table += "<tr><td class='cand'>#{c.nom} <br/><small>#{c.parti}</small></td>"
			html_table += "<td class='vote'>#{c.votes}</td></tr>"

		html_table += "</table>"


		$( '#myContent').html(html_table)

		inscrits = resultats[d.id].linscrits
		valides = resultats[d.id].lvalides
		if (valides > 0)
			thChart = d3.select('#myContent #chart')
			svg_donut = thChart.append('svg')
							.attr('id', 'svg_donut')
			svg_donut.append('text')
						.text("#{valides} / #{inscrits}")
						.attr("transform", "translate(110,117)")
						.attr("text-anchor", "middle")

			#ici on va utiliser le nest()
			console.log('building temp_array')
			temp_array = d3.nest()
					.key ((d) -> d.abreviation)
					.map(resultats[d.id].lcandidats)
			console.log(JSON.stringify(temp_array));

			qs  = temp_array['$qs'][0].votes
			plq = temp_array['$plq'][0].votes
			pq = temp_array['$pq'][0].votes
			caq = temp_array['$caq'][0].votes
			autres = valides - qs - plq - pq - caq

			deuxPi = 2.0 * Math.PI
			total_angle = 0.0

			#Rapport de surface d'un cercle par rapport à la surface
			#d'un autre cercle. Je dois extraire la racine carrée du
			#rapport entre abstentionistes/inscrits pour trouver le rayon
			#approprié.
			cercle_interne = Math.sqrt((inscrits - valides)/ inscrits) * 100

			arc_qs = d3.arc()
						.innerRadius(cercle_interne)
						.outerRadius(100)
						.startAngle(total_angle)
						.endAngle(total_angle + qs / valides * deuxPi)

			total_angle += qs / valides * deuxPi

			arc_plq = d3.arc()
						.innerRadius(cercle_interne)
						.outerRadius(100)
						.startAngle(total_angle)
						.endAngle(total_angle + plq / valides * deuxPi)

			total_angle += plq / valides * deuxPi

			arc_pq = d3.arc()
						.innerRadius(cercle_interne)
						.outerRadius(100)
						.startAngle(total_angle)
						.endAngle(total_angle + pq / valides * deuxPi)

			total_angle += pq / valides * deuxPi

			arc_caq = d3.arc()
						.innerRadius(cercle_interne)
						.outerRadius(100)
						.startAngle(total_angle)
						.endAngle(total_angle + caq / valides * deuxPi)

			total_angle += caq / valides * deuxPi

			arc_autres = d3.arc()
						.innerRadius(cercle_interne)
						.outerRadius(100)
						.startAngle(total_angle)
						.endAngle(total_angle + autres / valides * deuxPi)

			total_angle += autres / valides * deuxPi

			svg_donut.append('path')
				.attr("d", arc_qs)
				.attr("transform", "translate(110,117)")
				.attr("class", "resultat qs")

			svg_donut.append('path')
				.attr("d", arc_plq)
				.attr("transform", "translate(110,117)")
				.attr("class", "resultat plq")

			svg_donut.append('path')
				.attr("d", arc_pq)
				.attr("transform", "translate(110,117)")
				.attr("class", "resultat pq")

			svg_donut.append('path')
				.attr("d", arc_caq)
				.attr("transform", "translate(110,117)")
				.attr("class", "resultat caq")

			svg_donut.append('path')
				.attr("d", arc_autres)
				.attr("transform", "translate(110,117)")
				.attr("class", "resultat autres")




		else
			thChart = d3.select('#myContent #chart')
			thChart.html("<P>Aucun vote enregistré dans cette section</P>" +
			"<P>VotantEs inscritEs: #{inscrits}</P>")


		$( '#dialog' ).dialog(
			modal:true
			width: 600
			title: "Section de vote numéro: #{d.id}"
			buttons: {
				Ok: () ->
					$(this).dialog("close")
			})







	##	Data handlers
	#	-------------


	chargerFrontieresCirconscriptions = (data) ->
		#Setup boundries
		console.log("Limites des circonscriptions")
		didOnce = false
		try
			bfeature  = g.append("path")
			    .datum(topojson.mesh(data, data.objects.electionsQC3, (a, b) ->
					parti_a = d3.select(".d#{a.id}").property('classeParti')
					parti_b = d3.select(".d#{b.id}").property('classeParti')
					#console.log("#{a.properties.name} #{parti_a}  #{b.properties.name} #{parti_b}")
					a != b && parti_a == parti_b))
			    .attr("d", path)
			    .attr("class", "limite_circonscriptions memeParti");

			cfeature  = g.append("path")
			    .datum(topojson.mesh(data, data.objects.electionsQC3, (a, b) ->
					parti_a = d3.select(".d#{a.id}").property('classeParti')
					parti_b = d3.select(".d#{b.id}").property('classeParti')
					#console.log("#{a.properties.name} #{parti_a}  #{b.properties.name} #{parti_b}")
					a != b && parti_a != parti_b))
			    .attr("d", path)
			    .attr("class", "limite_circonscriptions partisDifferents");

		catch e
			console.log "ERROR:434\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
			return


	#

	##Pour les voisines, je colore selon le parti qui a gagné cette circonscription.

	chargerResultatsCirconstriptons = (data) ->
		d3.json "/resultats.json", (error, elections2014) ->
			if error
				console.log("#{ts()} Unable to load /resultats.json #{error}")
				alert('Incapable de charger /resulats.json')
				return
			else
				console.log("#{ts()} Resultats QC reçus")
				circonscriptions = elections2014.circonscriptions
				console.log("#{ts()} circonscriptions loaded (#{circonscriptions.length})")
				for circonscription, i in circonscriptions
					numero = circonscription.numeroCirconscription
					#On ne veut pas d'attribution d'unE gagnantE dans Lévis
					#puiqu'il s'agit de la circonscription qui affichera les résultats
					#section de vote par section de vote.
					if (numero == 663)			#Si c'est Sainte-Marie — Saint-Jaques, je passe par dessus
						continue

					candidats = circonscription.candidats			#liste des candidatEs pour une circonscriptions
					partiElu = candidats[0].numeroPartiPolitique	#le ou la candidatE en position 0 a gagné
					partiSecond = candidats[1].numeroPartiPolitique	#le ou la candidatE en position 1 est 2ème
					classeParti = ""								#va contenir la classe (CSS) attribuée au path

					if partiElu > 0
						switch partiElu
							when 6 then classeParti = "plq"		#ces numéros sont attribués par le DGEQ
							when 8 then classeParti = "pq"		#et leur attribution est présente dans le fichier JSON
							when 11 then classeParti = "qs"
							when 27 then classeParti = "caq"
							else classeParti = "autreParti"


						repere = "path\##{numero}"				#je repère le 'path' correspondant au tracé de la circonscription
						#console.log("  >>> Assigning #{classeParti} to #{repere} - #{circonscription.nomCirconscription}")
						if ($(repere) && $(repere)[0])
							$(repere)[0]
								.setAttribute("class", "district d#{numero} elu #{classeParti}")
							try
								d3.select(".d#{numero}").property({'partiElu': partiElu,'partiSecond': partiSecond, 'classeParti' : classeParti })
								#d3test = d3.select(".d#{numero}").property('partiElu')
								#console.log("#{numero}")
								#console.log("#{d3test}")
							catch e
								console.error("EXCEPTION d3.select(d#{numero}) #{e.message}")

						#Une fois les tracés de surface faits (data) et les résultats connus
						#on peut charger le tracé final des limites de circonscriptions
				chargerFrontieresCirconscriptions(data)
	#

	#Charger les résultats bureau par bureau lors
	#des élections du 2014-04-07.
	#Source: DGEQ http://www.electionsquebec.qc.ca/documents/zip/resultats-section-vote/2014-04-07.zip
	#Heroku a du mal avec les noms comportant des caractères accentués, des espaces etc. il vaut mieux
	#renommer le fichier d'une manière qui lui soit moins pénible. Le fichier zip du DGEQ contient 125
	#fichiers, un par circonscription.
	chargerResultatsParSections = () ->
		#dsv = d3.dsv(";", "text/plain");			#le séparateur est un point-virgule (';')
		d3.text "/smsj.csv", (error, smsj2014_txt) ->
			if error
				console.log("#{ts()} Unable to load /smsj.json #{error}")
				alert("Réception des données impossibles. Vérifiez la console pour plus de détails")
				return
			else
				dsv = d3.dsvFormat(';')
				smsj2014 = dsv.parse(smsj2014_txt)
				console.log("#{ts()} Resultats Saint-Marie – Saint-Jacques reçus reçus")
				for section, i in smsj2014
					sv = section['S.V.']
					inscrits = parseInt(section['É.I.'],10)
					valides = parseInt(section['B.V.'], 10)
					section.candidats = []
					for c, j in liste_candidats
						section.candidats.push {
							clef: c.clef
							nom: c.nom
							parti: c.parti
							abreviation: c.abreviation
							votes: parseInt(section[c.clef],10)
						}

					#Certaines sections de votes ont été subdivisées parce que le
					#nombre d'électeurTRICEs dépassait 300. Si le résultat parle de
					#secA et secB, sur la carte, il n'y a qu'une seule section 'sec'
					#il convient donc d'additionner les deux résultats pour pouvoir
					#avoir un affichage satisfaisant.

					if (sv in ['121A'])
						sv = sv.split('A')[0]
						suivante = smsj2014[i+1]
						inscrits += parseInt(suivante['É.I.'],10)
						valides += parseInt(suivante['B.V.'],10)
						for c, j in liste_candidats
							section.candidats[j].votes += parseInt(suivante[c.clef],10)


					#Trier les candidatEs par nombre de vote (desc)
					section.candidats = section.candidats.sort (a,b) ->
						b.votes - a.votes

					partiGagnant = section.candidats[0].abreviation
					partiSecond = section.candidats[1].abreviation
					partiSecondVoix = section.candidats[1].votes
					voteMaj = section.candidats[1].votes
					classeA = "S/O"

					#Va être utilisé pour repérer la section (un 'path' dans svg)
					repere = "path\#s#{sv}"
					if ($(repere) && $(repere)[0])		#toujour vérifier si cet objet existe réellement (jquery et DOM)

						classeA = "S/O"
						target = $(repere)[0]
						#je teste toutes les combinaisons entre les 4 premiers partis
						if partiSecond == 'qs'
							if partiGagnant == 'caq'
								classeA =  "challengeQSCAQ"
							if partiGagnant == 'plq'
								classeA = "challengeQSPLQ"
							if partiGagnant == 'pq'
								classeA = "challengeQSPQ"

							target.setAttribute("class", "section s#{sv} challenge #{classeA}")

						else if partiSecond == 'pq'
							if partiGagnant == 'caq'
								classeA =  "challengePQCAQ"
							if partiGagnant == 'plq'
								classeA = "challengePQPLQ"
							if partiGagnant == 'qs'
								classeA = "challengePQQS"

							target.setAttribute("class", "section s#{sv} challenge #{classeA}")

						else if partiSecond == 'plq'
							if partiGagnant == 'caq'
								classeA =  "challengePLQCAQ"
							if partiGagnant == 'pq'
								classeA = "challengePLQPQ"
							if partiGagnant == 'qs'
								classeA = "challengePLQQS"

							target.setAttribute("class", "section s#{sv} challenge #{classeA}")

						else if partiSecond == 'caq'
							if partiGagnant == 'plq'
								classeA = "challengeCAQPLQ"
							if partiGagnant == 'pq'
								classeA = "challengeCAQPQ"
							if partiGagnant == 'qs'
								classeA = "challengeCAQQS"
							target.setAttribute("class", "section s#{sv} challenge #{classeA}")
						else
							$(repere)[0]
								.setAttribute("class", "section s#{sv} gagnant #{partiGagnant}")
							#console.log section

						#Je garde une trace de cette section avec les résultats complets pour
						#l'affichage des résulats complets.
						res =
							lsv: sv					#section de vote
							linscrits: inscrits		#électeurTRICEs inscritEs
							lvalides: valides		#total des votes valides
							lpremier: partiGagnant	#parti qui arriver 1er dans cette section
							lsecond: partiSecond	#parti qui arrive 2ème dans cette section
							lclasseA: classeA		#classe (CSS) utilisée pour colorier la section
							lcandidats: section.candidats

						resultats[sv] = res			#resulats[] est une globle, indexée avec le no de section de vote.






	#

	#Obtenir les données GeoJSON/TopoJSON décrivant les limites des circonscriptions
	#voisines et les sections de vote dans la circonscription. Ceci est appellé en
	#premier et on doit s'assurer que tous les objets créés ici sont prêts avant de
	#charger les fichiers de résultats
	chargerTopoJSON = () ->
		d3.json "/smsj.json", (error, data) ->
			if (error)
				console.log("#{ts()} error loading data #{error}")
				alert("Probème de réception des fichiers de contours")
				return
			else
				console.log("#{ts()} smsj.json (sections de vote - tracé des limites) reçu. ")

				#Les circonscriptions voisines de Sainte-Marie=Saint-Jacques
				voisines = topojson.feature(data, data.objects.electionsQC3)

				#Les sections de vote dans SMSJ
				smsj = topojson.feature(data, data.objects.smsj)

				#Chaque fois que le niveau de zoom change, il faut tout remettre
				#à sa place compte tenu du nouveau contexte géographique
				reset = () ->
					##Chaque fois que l'usagerE zoom, il faut recalculer
					##les positions relatives de chaque élément en surimposition
					##pour bien les positionner sur la carte.


					##Reset the corners of the svg
					bounds = path.bounds(voisines)
					topLeft = bounds[0]
					bottomRight = bounds[1]
					svg.attr("width", bottomRight[0] - topLeft[0])
						.attr("height", bottomRight[1] - topLeft[1])
						.style("left", topLeft[0] + "px")
						.style("top", topLeft[1] + "px")
					g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")")

					#Le svg prend classe selon le zoom: z9, z10, z11 .. z18
					#Voir smsj.css.scss pour voir l'effet (aka certains éléments invisibles)
					svg.attr("class", "z#{map.getZoom()}")

					## Redessiner différents éléments avec le nouveau système de référence
					## déterminé en fonction du zoom.

					#Contenu des circonscriptons
					feature.attr("d", path)

					#bordure des circonscription (même parti des deux côtés)
					if bfeature?
						bfeature.attr("d", path)

					#bordure des circonscripton (partis différents de chaque côté)
					if cfeature?
						cfeature.attr("d", path)
					sections.attr("d", path)

					#Les noms des circonscriptons doivent être positionnées
					#j'en profite pour montrer/cacher ou changer la taille du
					#caractère pour ne pas nuire à la visibilité
					if (noms != null)
						noms
							.attr("transform", (d) ->
								#Je trouve le centroid de la circonscription
								#(lon,lat) et je transforme ce point dans le
								#système de coordonnées du moment (qui varie selon
								#le zoom)
								centroid = map.latLngToLayerPoint(d.latLng)
								"translate(#{centroid.x}, #{centroid.y})")
							.attr("visibility", (d) ->
								#Je vérifie la surface (area) de la circonscription
								#selon le zoom du moment. Si la valeur passe en dessous
								#d'un certaine valeur, ja cache le nom de la circonscription
								rep = "novalue"
								if path.area(d)<6000.0
									rep = "hidden"
								else
									#Ici je n'indiue pas "visible" parce que
									#toute la couche est masquée pendant certaines
									#operations. En utilisant 'inherit', si la couche
									#est visible, le nom de la circonscription le sera
									#aussi mais si la couche n'est pas visible, le
									#nom de la circonscription en fera de même
									rep = "inherit"

								#console.log("#{d.properties.name} area: #{path.area(d)}  ––  #{rep}")
								rep)

							.attr("district_area", (d) ->
								path.area(d))

							.style("font-size", (d) ->
								#ici aussi j'utilise le surface de la circonscription
								#dans le contexte du zoom pour déterminer la taille des
								#caractères
								rep = "3pt"
								if path.area(d) >= 25000.0
									rep = "18pt"
								else if path.area(d)>6500.0
									rep = "12pt"
								else
									rep = "6pt"

								rep)

					#Meme chose pour les numéros de sections avec cependant
					#d'autres valeurs pour déterminer la visibilité ou la taille
					#des caractères
					if (numSecs != null)
						numSecs
							.attr("transform", (d) ->
								centroid = map.latLngToLayerPoint(d.latLng)
								"translate(#{centroid.x}, #{centroid.y})")
							.attr("visibility", (d) ->
								rep = "novalue"
								if path.area(d)<1000.0
									rep = "hidden"
								else
									rep = "inherit"

								#console.log("#{d.properties.name} area: #{path.area(d)}  ––  #{rep}")
								rep)
							.style("font-size", (d) ->
								rep = "3pt"
								if path.area(d) >= 10000.0
									rep = "15pt"
								else if path.area(d) >= 6000.0
									rep = "12pt"
								else if path.area(d)>3000.0
									rep = "9pt"
								else
									rep = "6pt"

								rep)






					# Fin de la méthode 'reset'
					#
				#
				#d3 permet de régler un mécanisme de transformation entre le
				#system de référence qui lui est propre et celui utilisé par
				#la carte Leaflet.
				transform = d3.geoTransform({point: projectPoint})
				path = d3.geoPath().projection(transform)

				# J'ajoute les limites des circonscriptions
				feature = g.selectAll(".district")
					.data(voisines.features)
					.enter()
					.append("path")
						.attr("id", (d) ->
							d.id)					#le id de la circonscription n'a aucun préfixe
						.attr("class", (d) ->
							"district d" + d.id)
						.attr("name", (d) ->
							d.properties.name + " QC")


				# Dans smsj, je trace les limites des sections de vote
				sections = g.selectAll(".section")
					.data(smsj.features)
					.enter()
					.append("path")
						.attr("class", (d) ->
							"section s" + d.id)
						.attr("id", (d) ->
							"s" + d.id)				#le id de la section est précédé de "s"



				#J'ajoute le nom des circonscriptions voisines
				noms = g.selectAll("text.nom")
							.data(voisines.features)
							.enter()
							.append("text")
								.attr("id", (d) ->
									"n" + d.id)		#je précède le numéro de circonscription de "n"
													#afin qu'il n'y ait pas deux object avec le même id.
								.text((d) ->
									d.properties.name)
								.attr("district_area", "nil")
								.attr("class", "nom")
								.attr("text-anchor", "middle")
								.attr("transform", (d) ->
									centroid = path.centroid(d)						#je trouve le centroid de la circonscription
									d.latLng = map.layerPointToLatLng(centroid )	#que je traduit dans le système de référence Leaflet
									"translate(#{centroid[0]}, #{centroid[1]})")	#et je l'enregistre avec 'd'. Et je déplace le nom au bon endroit


				#Dans Sainte-Marie — Saint-Jacques, j'ajoute les numéros de section
				numSecs = g.selectAll("text.numSec")
								.data(smsj.features)
								.enter()
								.append("text")
									.attr("id", (d) ->
										"sn" + d.id)				#ici aussi je met "sn" en préfixe pour éviter les duplicata d'id.
									.text((d) ->
										d.id)
									.attr("class", "numSec")
									.attr("text-anchor", "middle")
									.attr("transform", (d) ->
										centroid = path.centroid(d)
										d.latLng = map.layerPointToLatLng(centroid )
										"translate(#{centroid[0]}, #{centroid[1]})")

				#make sure when the map is zoomed, the reset is called, and
				#call reset anayway to complete the district drawing process
				map.on('zoomend', reset);			#J'associe un 'handler' à un évennement de la 'map' (changement de zoom)
				reset();							#et j'appelle manuellement ce 'handler' pour parfaire le visuel

				startTime = null			#Une globale voir "touchstart" et "touchend"

				#Pour le iPad ou le iPhone (ou Android/Blackberry ou autre mobile)
				sections.on("touchstart", (d) ->
					startTime = new Date().getTime()			#je règle le startTime au moment présent
				)

				sections.on("touchend", (d) ->
					dur = new Date().getTime() - startTime		#j'observe pendant combien de temps le doigt est resté sur la section
					if (dur > 1500)								#si plus que 1500 milli-secondes, alors on fait quelquechose
						startTime *= 2
						setUpDialog(d)							#je lance l'affichage de la fenêtre d'information complète
				)

				sections.on("mousedown", (d) ->
					console.log("#{ts()} click on section. ")
					#Si 'alt' ne pas pressé, j'avorte
					if !d3.event.altKey							#pour les ordinateurs, la touche 'alt' sert à faire démarrer l'affichage
						console.log("#{ts()} no altKey ")
						return									#de la fenêtre d'information complète
					else
						console.log("#{ts()} altKey ")
						setUpDialog(d)
				)



				console.log("#{ts()} Constituencies drawing completed!")

				##Chargement des résultats
				chargerResultatsCirconstriptons(data)
				chargerResultatsParSections()


				#Fin chargerTopoJSON




	#


	## ###############################################################################################
	##
	##	Point d'entrée
	##
	## ###############################################################################################

	##	Création de la carte de base
	#	----------------------------

	#Créer la carte en arrière plan en utilisant OpenStreetMap.
	#Voir http://leafletjs.com/reference.html
	map = new L.Map("map", {
			center: [45.518, -73.547], 				# Centre de la carte (latitude, longitued)
			zoom: 13,								# Niveau de zoom au démarage
			minZoom: 11								# Niveau de zoom minimum (minimum: 0)
			maxZoom: 17								# Niveau de zoom maxumum (maximum: 18)
			maxBounds: [[45.3,-74], [46, -73]]		# Zone pouvant être affichée en déplaçant la carte
		})
		.addLayer(new L.TileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png",
			{	attribution: "&copy; Les contributeurRICEs d'<a href=\"http://osm.org/copyright\">OpenStreetMap</a>  " +
				"| &copy <a href=\"http://www.electionsquebec.qc.ca\">Le Directeur général des élections du Québec</a>"}))

	console.log("#{ts()} Carte Leaflet crée.")


	#Créer la sur-impression avec les données cartographique du DGEQ
	svg = d3.select(map.getPanes().overlayPane).append("svg")

	setUpPatterns()

	#Ajouter un groupe (g) au SVG.
	g = svg.append("g").attr("class", "leaflet-zoom-hide")

	console.log("#{ts()} Couche d3 créé. Chargement des donnés")
	chargerTopoJSON()
