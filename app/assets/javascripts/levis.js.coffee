#
# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
	console.log("Lévis ...")
	$('body').addClass('levisjs_loaded')
	
	#Carte de la circonscription en la divisant
	#par section de vote (en date du 2014-04-07)
	#Source: DGEQ http://www.electionsquebec.qc.ca/documents/zip/sections%20de-vote-elections-2014-shapefile.zip
	
	###
	Note:	Ce fichier zip comprend un répertoire de plusieurs 
			fichiers et le fichier *.shp à lui 
			seul occupe pas moins de 44Mo. En utilisant les 
			outils GDAL (ogr2org) je transforme cet 
			ensemble de fichiers en un fichier GeoJSON puis 
			topoJSON en faisant l'extraction des données
			concernant uniquemet la circonscription de Lévis 
			(code 663 selon le DGEQ)
	
			Comme le fichier est encodé en utilisant le vieux 
			format ISO-8859-1 (Latin1), il est important
			de le réencoder en utilisant le standard Unicode (UTF8). 
			Pour ce faire, il faut indiquer que
			la source (Sections\ de\ Vote\ Elections\ 2014_04_07.shp) 
			est encodée ISO-8859-1:
	
			export SHAPE_ENCODING="ISO-8859-1"
	
			Ensuite on fait tourner l'utilitaire ogr2ogr pour 
			extraire les données, les réencoder en format
			UTF8 et sauvegarder le tout en format GeoJSON
	
			ESRI: 4326 http://spatialreference.org/ref/epsg/wgs-84/
			ogr2ogr: http://www.gdal.org/ogr2ogr.html
	
			ogr2ogr -f GeoJSON -where "CO_CEP = '663'" \
				-t_srs EPSG:4326 Levis.json \
				Sections\ de\ Vote\ Elections\ 2014_04_07.shp \
				-lco ENCODING=UTF-8
	
			Enfin, la commande: ogrinfo -al -geom=summary Levis.json | less
			permet de jeter un coup d'oeil au fichier créé est constater:
	
			1-	Que les caractères accentuées on été ré-encodés en 
				UTF-8 est sont lisibles (Lévis)
			2-	Les références utilisent des longitudes et latitudes 
				(le -t_srs EPSG:4326 en est reponsable)
			3-	Seuls les données de Lévis sont dans ce 
				fichier (-where "CO_CEP='663'")
	
			N'oubliez pas de mettre l'option -geom=summary (ou -geom=no) 
			si ne voulez pas être submergéE par les données géométriques 
			(qui sont très abondantes mais pas vraiment lisibles par 
			un humain).
	
			Le fichier Levis.json pèse maintenant 288Ko (au lieu les 46Mo 
			à l'origine), mais il y a moyen de faire mieux. N'oublions pas 
			que ce fichier va transiter sur Internet et son temps de 
			transmission s'ajoutera au temps que prendra Javascript 
			pour dessiner la carte.
	
			J'aimerais cependant avoir les limites des circonscriptions 
			voisines. Mon but est d'avoir les limites des différentes 
			circonscriptions qui entourent Lévis et à l'intérieur de Lévis
			avoir les limites de chaque section de vote.
	
			L'ensemble des limites des circonscriptions est disponibles 
			sur le site du DGEQ ici:
	
			http://www.electionsquebec.qc.ca/documents/zip/circonscriptions-electorales-2011-shapefile-v2.zip
	
			Encore une fois, il convient de convertir les 
			données qui à l'origine utilisent l'encodage
			ISO-8859-1 (Latin1) vers l'Unicode (UTF-8). De plus, 
			je n'ai pas besoin des limites de toutes
			les circonscriptions du Québec, seulement les 
			circonscriptions voisines de Lévis. 
	
			ogr2ogr	-f GeoJSON  \
					-t_srs EPSG:4326 \
					-clipdst -72.172 46.34 -70.172 47.34 \
					-overwrite \
					../electionsQC2.json carte.shp \
					-lco ENCODING=UTF-8 
	
			Note: Cette commande produit une erreur (exception) 
			et est incapable de rendre la circonscription de 
			Chute-de-la-Chaudière. Pour pallier à ce problème
			j'ai remplacé le "-clipdst ..." par "-where CO_CEP in (...)", le
			... étant la liste des circonscriptions qui doivent être
			incluses dans le fichier final. 24 au total.
	
	
	
			Et nous terminons avec topojson pour combiner à la fois les 
			limites des circonscriptions de la région et les limites 
			des sections de vote dans Lévis:
	
			topojson 	-o ~/Documents/Development/myapp/public/levis.json  \
						-q 1e6 \
						--id-property CO_CEP,NO_SV \
						--properties name=NM_CEP \
						--shapefile-encoding utf8 \
						--  electionsQC2.json Levis.json
	
			où
			-o				nom du fichier de destination
			-q				quand tourner les coins ronds
			--id-property	Quel élément servira de id
			--properties	Les propriétés à conserver 
							(NM_CEP deviendra 'name')
			--				fichiers devant être combinés
	###
	
	#Variables globales
	svg = null				#l'élément svg qui servira de contenant
	path = null				#contiendra l'object servant à assurer 
							# une bonne projection
	map	= null				#l'objet qui contiendra la carte
	feature = null			#les tracés des circonscriptions
	sections = null			#les tracés des sections
	numSecs = null			#les noms des circonscriptons

	##Fonctions utilitaires
	
	#Utilisé pour transformer les coordonnés d'un point
	#de Lat/Long au système de coordonné local
	projectPoint = (x, y) ->
		point = map.latLngToLayerPoint(new L.LatLng(y, x))
		this.stream.point(point.x, point.y)

	#Sert à imprimer les secondes et millisecondes (timestamp)
	#pour vérifier les performances d'exécution (pour du débug)
	ts = () ->
		moment().format("ss.SSS")
		
	addPattern = (name, backgroundColor, challengeColor) ->
		pat = svg.append("pattern")
			.attr("id", name)
			.attr("patternUnits", "userSpaceOnUse")
			.attr("width", "10")
			.attr("height", "10")
		
		
		pat.append('rect')
				.attr("width", "10")
				.attr("height", "10")
				.attr("fill", backgroundColor)
		
			
		pat.append("path")
				.attr("d", "M0,0 l10,10 ")
				.attr("stroke", challengeColor)
				.attr("stroke-width", "1.5")
				.attr("stroke-linecap", "butt")		
		
	#Créer la carte en arrière plan en utilisant OpenStreetMap.
	map = new L.Map("map", {center: [46.78, -71.172], zoom: 12})
		.addLayer(new L.TileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png",
			{	attribution: "&copy; Les contributeurRICEs d'<a href=\"http://osm.org/copyright\">OpenStreetMap</a>  " +
				"| &copy <a href=\"http://www.electionsquebec.qc.ca\">Le Directeur général des élections du Québec</a>"}))
	
	console.log("#{ts()} Carte Leaflet crée.")
	

	#Créer la sur-impression avec les données cartographique du DGEQ
	svg = d3.select(map.getPanes().overlayPane).append("svg")
	
	
	##Créer un pattern
	addPattern("qsChallengeCAQ", "rgba(132,112,255,0.5)", "orange")
	addPattern("qsChallengePLQ", "rgba(255,0,0,0.5)", "orange")
	addPattern("qsChallengePQ", "rgba(0,0,128,0.5)", "orange")

	addPattern("pqChallengeCAQ", "rgba(132,112,255,0.5)", "rgba(0,0,128,0.5)")
	addPattern("pqChallengePLQ", "rgba(255,0,0,0.5)", "rgba(0,0,128,0.5)")
	addPattern("pqChallengeQS", "orange", "rgba(0,0,128,0.5)")

	addPattern("plqChallengeCAQ", "rgba(132,112,255,0.5)", "rgba(255,0,0,0.5)")
	addPattern("plqChallengePQ", "rgba(0,0,128,0.5)", "rgba(255,0,0,0.5)")
	addPattern("plqChallengeQS", "orange", "rgba(255,0,0,0.5)")
		
	addPattern("caqChallengePQ", "rgba(0,0,128,0.5)", "rgba(132,112,255,0.5)")
	addPattern("caqChallengePLQ", "rgba(255,0,0,0.5)", "rgba(0,0,128,0.5)")
	addPattern("caqChallengeQS", "orange", "rgba(0,0,128,0.5)")
		
	g = svg.append("g").attr("class", "leaflet-zoom-hide")
			
	
	
	console.log("#{ts()} Couche d3 créé. Chargement des donnés")
	
	#Get the overlay data used to draw constituencies lines
	d3.json "/levis.json", (error, data) ->
		if (error)
			console.log("#{ts()} error loading data #{error}")
			alert("Probème de réception des fichiers de contours")
			return
		else
			console.log("#{ts()} levis.json (sections de vote - tracé des limites) reçu. ")
			
			#Les circonscriptions voisines de Lévis
			voisines = topojson.feature(data, data.objects.electionsQC2)

			#Les sections de vote dans Lévis
			levis = topojson.feature(data, data.objects.Levis)
			
			reset = () ->
				##Chaque fois que l'usagerE zoom, il faut recalculer
				##les positions relatives de chaque élément en surimposition
				##pour bien les positionner sur la carte.
				
				
				##Reset the corners of the svg 
				bounds = path.bounds(voisines)
				topLeft = bounds[0]
				bottomRight = bounds[1]
				svg.attr("width", bottomRight[0] - topLeft[0])
					.attr("height", bottomRight[1] - topLeft[1])
					.style("left", topLeft[0] + "px")
					.style("top", topLeft[1] + "px")
				g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")")
			
				## Redraw the distrist borders using a new zoomed projection
				feature.attr("d", path)
				sections.attr("d", path)
			
				#Les noms des circonscriptons doivent être positionnées
				#j'en profite pour montrer/cacher ou changer la taille du
				#caractère pour ne pas nuire à la visibilité
				if (noms != null)
					noms
						.attr("transform", (d) ->
							#Je trouve le centroid de la circonscription
							#(lon,lat) et je transforme ce point dans le
							#système de coordonnées du moment (qui varie selon
							#le zoom)
							centroid = map.latLngToLayerPoint(d.latLng)
							"translate(#{centroid.x}, #{centroid.y})")
						.attr("visibility", (d) ->
							#Je vérifie la surface (area) de la circonscription
							#selon le zoom du moment. Si la valeur passe en dessous
							#d'un certaine valeur, ja cache le nom de la circonscription
							rep = "novalue"
							if path.area(d)<3000.0 
								rep = "hidden"
							else
								#Ici je n'indiue pas "visible" parce que
								#toute la couche est masquée pendant certaines
								#operations. En utilisant 'inherit', si la couche
								#est visible, le nom de la circonscription le sera
								#aussi mais si la couche n'est pas visible, le
								#nom de la circonscription en fera de même
								rep = "inherit"
							
							#console.log("#{d.properties.name} area: #{path.area(d)}  ––  #{rep}")
							rep)

						.attr("district_area", (d) ->
							path.area(d))

						.style("font-size", (d) ->
							#ici aussi j'utilise le surface de la circonscription
							#dans le contexte du zoom pour déterminer la taille des
							#caractères
							rep = "3pt"
							if path.area(d) >= 13000.0
								rep = "18pt"
							else if path.area(d)>8000.0
								rep = "12pt"
							else
								rep = "6pt"
							
							rep)
							
				#Meme chose pour les numéros de sections avec cependant
				#d'autres valeurs pour déterminer la visibilité ou la taille
				#des caractères
				if (numSecs != null)
					numSecs
						.attr("transform", (d) ->
							centroid = map.latLngToLayerPoint(d.latLng)
							"translate(#{centroid.x}, #{centroid.y})")
						.attr("visibility", (d) ->
							rep = "novalue"
							if path.area(d)<1000.0 
								rep = "hidden"
							else
								rep = "inherit"
							
							#console.log("#{d.properties.name} area: #{path.area(d)}  ––  #{rep}")
							rep)
						.style("font-size", (d) ->
							rep = "3pt"
							if path.area(d) >= 6000.0
								rep = "9pt"
							else if path.area(d)>3000.0
								rep = "6pt"
							else
								rep = "4pt"
							
							rep)
						
							
							
			#setup the transformation mecanism
			transform = d3.geo.transform({point: projectPoint})
			path = d3.geo.path().projection(transform)
			
			# J'ajoute les limites des circonscriptions
			feature = g.selectAll(".district")
				.data(voisines.features)
				.enter()
				.append("path")
					.attr("id", (d) ->
						d.id)
					.attr("class", (d) ->
						"district d" + d.id)
					.attr("name", (d) ->
						d.properties.name + " QC")
						


			# Dans Lévis, je trace les limites des sections de vote
			sections = g.selectAll(".section")
				.data(levis.features)
				.enter()
				.append("path")
					.attr("class", (d) ->
						"section s" + d.id)
					.attr("id", (d) ->
						"s" + d.id)
						
			
			#Le nom des circonscriptions
			noms = g.selectAll("text.nom")
						.data(voisines.features)
						.enter()
						.append("text")
							.attr("id", (d) ->
								"n" + d.id)
							.text((d) ->
								d.properties.name)
							.attr("district_area", "nil")
							.attr("class", "nom")
							.attr("text-anchor", "middle")
							.attr("transform", (d) ->
								centroid = path.centroid(d)
								d.latLng = map.layerPointToLatLng(centroid )
								"translate(#{centroid[0]}, #{centroid[1]})")
								

			#Dans Lévis, les numéros de section
			numSecs = g.selectAll("text.numSec")
							.data(levis.features)
							.enter()
							.append("text")
								.attr("id", (d) ->
									"sn" + d.id)
								.text((d) ->
									d.id)
								.attr("class", "numSec")
								.attr("text-anchor", "middle")
								.attr("transform", (d) ->
									centroid = path.centroid(d)
									d.latLng = map.layerPointToLatLng(centroid )
									"translate(#{centroid[0]}, #{centroid[1]})")
			
			#make sure when the map is zoomed, the reset is called, and
			#call reset anayway to complete the district drawing process		
			map.on("viewreset", reset);
			reset();
		
			console.log("#{ts()} Constituencies drawing completed!")

			##Chargement des résultats
			
			##Pour les voisines
			d3.json "/resultats.json", (error, elections2014) ->
				if error 
					console.log("#{ts()} Unable to load /resultats.json #{error}")
					alert('Incapable de charger /resulats.json')
					return
				else
					console.log("#{ts()} Resultats QC reçus")
					circonscriptions = elections2014.circonscriptions
					console.log("#{ts()} circonscriptions loaded (#{circonscriptions.length})")
					for circonscription, i in circonscriptions 
						numero = circonscription.numeroCirconscription
						#On ne veut pas d'attribution d'unE gagnantE dans Lévis
						#puiqu'il s'agit de la circonscription qui affichera les résultats
						#section de vote par section de vote.
						if (numero == 663)
							continue
						candidats = circonscription.candidats
						partiElu = candidats[0].numeroPartiPolitique
						partiSecond = candidats[1].numeroPartiPolitique
						classeParti = ""
			
						if partiElu > 0
							switch partiElu
								when 6 then classeParti = "plq"
								when 8 then classeParti = "pq"
								when 11 then classeParti = "qs"
								when 27 then classeParti = "caq"
								else classeParti = "autreParti"
							repere = "path\##{numero}"
							#console.log("  >>> Assigning #{classeParti} to #{repere} - #{circonscription.nomCirconscription}")
							if ($(repere) && $(repere)[0])
								$(repere)[0]
									.setAttribute("class", "district d#{numero} elu #{classeParti}")

					#Charger les résultats bureau par bureau lors
					#des élections du 2014-04-07.
					#Source: DGEQ http://www.electionsquebec.qc.ca/documents/zip/resultats-section-vote/2014-04-07.zip
	
					dsv = d3.dsv(";", "text/plain");
					dsv "/Levis.csv", (error, levis2014) ->
						if error 
							console.log("#{ts()} Unable to load /resultats.json #{error}")
							alert("Réception des données impossibles. Vérifiez la console pour plus de détails")
							return
						else
							console.log("#{ts()} Resultats Lévis reçus")
							for section,i in levis2014
								sv = section['S.V.']
								onPiq = parseInt(section['Belley Nicolas O.N. - P.I.Q.'],10)
								pun = parseInt(section['Biron Paul P.U.N.'],10)
								qs = parseInt(section['Bonnier Viger Yv Q.S.'],10)
								caq = parseInt(section['Dubé Christian C.A.Q.-É.F.L.'],10)
								pq = parseInt(section['Girard Sylvie P.Q.'],10)
								eapPcq = parseInt(section['Roy Sébastien É.A.P. - P.C.Q.'],10)
								plq = parseInt(section['Turmel Simon P.L.Q./Q.L.P.'],10)
						
								#Certaines sections de votes ont été subdivisées parce que le
								#nombre d'électeurTRICEs dépassait 300. Si le résultat parle de
								#secA et secB, sur la carte, il n'y a qu'une seule section 'sec'
								#il convient donc d'additionner les deux résultats pour pouvoir
								#avoir un affichage satisfaisant.
								if (sv in ['37A', '41A', '54A', '114A', '127A'])
									sv = sv.split('A')[0]
							
									onPiq += parseInt(levis2014[i+1]['Belley Nicolas O.N. - P.I.Q.'],10)
									pun += parseInt(levis2014[i+1]['Biron Paul P.U.N.'],10)
									qs += parseInt(levis2014[i+1]['Bonnier Viger Yv Q.S.'],10)
									caq += parseInt(levis2014[i+1]['Dubé Christian C.A.Q.-É.F.L.'],10)
									pq += parseInt(levis2014[i+1]['Girard Sylvie P.Q.'],10)
									eapPcq += parseInt(levis2014[i+1]['Roy Sébastien É.A.P. - P.C.Q.'],10)
									plq += parseInt(levis2014[i+1]['Turmel Simon P.L.Q./Q.L.P.'],10)
						
								partiGagnant = "SO"
								partiSecond = "SO"
								partiSecondVoix = 0
								voteMaj = 0
								
								challenged = ['37', '45', '46', '48', '50', '51', '57', '61', '67', '122']
								
								
								if onPiq > voteMaj
									voteMaj = onPiq
									partiGagnant = "on"
								
								if pun > voteMaj
									voteMaj = pun
									partiGagnant = "pun"
								else if pun > partiSecondVoix
									partiSecondVoix = pun
									partiSecond = 'pun'
									
								if qs > voteMaj
									voteMaj = qs
									partiGagnant = "qs"
								else if qs > partiSecondVoix
									partiSecondVoix = qs
									partiSecond = 'qs'	
								
								if caq > voteMaj
									voteMaj = caq
									partiGagnant = "caq"
								else if caq > partiSecondVoix
									partiSecondVoix = caq
									partiSecond = 'caq'
								
								if pq > voteMaj
									voteMaj = pq
									partiGagnant = "pq"
								else if plq > partiSecondVoix
									partiSecondVoix = pq
									partiSecond = 'pq'
									
								if eapPcq > voteMaj
									voteMaj = eapPcq
									partiGagnant = "pcq"
								else if eapPcq > partiSecondVoix
									partiSecondVoix = eapPcq
									partiSecond = 'pcq'
									
								if plq > voteMaj
									voteMaj = plq
									partiGagnant = "plq"
								else if plq > partiSecondVoix
									partiSecondVoix = plq
									partiSecond = 'plq'
							
								if (sv in challenged)
									console.log(">>> #{sv}: #{partiGagnant} #{voteMaj}  2nd: #{partiSecond} #{partiSecondVoix} – " +
									"caq: #{caq} plq:#{plq} pq:# #{pq} qs: #{qs}")
									
								repere = "path\#s#{sv}"
								if ($(repere) && $(repere)[0])
									if partiSecond == 'qs'
										if partiGagnant == 'caq' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengeQSCAQ")
										if partiGagnant == 'plq' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengeQSPLQ")
										if partiGagnant == 'pq' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengeQSPQ")
									else if partiSecond == 'pq'
										if partiGagnant == 'caq' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengePQCAQ")
										if partiGagnant == 'plq' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengePQPLQ")
										if partiGagnant == 'qs' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengePQQS")
									else if partiSecond == 'plq'
										if partiGagnant == 'caq' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengePLQCAQ")
										if partiGagnant == 'pq' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengePLQPQ")
										if partiGagnant == 'qs' 
											$(repere)[0]
												.setAttribute("class", "section s#{sv} challenge challengePLQQS")
										
									else
										$(repere)[0]
											.setAttribute("class", "section s#{sv} gagnant #{partiGagnant}")
										#console.log section
											
	