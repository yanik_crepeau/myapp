$ ->
	##Gobal variable
	db = null
	
	#Utility function
	printDBInformation = (idb) ->
		strNames = "no idb"
		if idb
			sName = idb.name
			dVersion = idb.version
			dTableNames = idb.objectStoreNames
			strNames = "*** IndexedDB name: " + sName + "; version: " + dVersion + "; object stores: "
			for name in dTableNames
				strNames = strNames + name + ", "

		console.log(strNames)
	

	ts = () ->
		moment().format("ss.SSS")

	
	# In the following line, you should include the prefixes of implementations you want to test.
	window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

	# DON'T use "var indexedDB = ..." if you're not in a function.
	# Moreover, you may need references to some window.IDB* objects:
	window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
	window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
	# (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
	
	if(window.indexedDB)
		console.log("#{ts()}: indexedDB is supported")
		request = null
		shouldAddInitialData = false
		pageSize  = 100000
		sizeLimit = 5000000
		myStack= []
		
		addData = () ->
			shouldContinue  = true
			fromIndex = myStack.pop()
			try
				console.log("#{ts()}: Setting up transaction #{fromIndex.min}")
				transaction2 = db.transaction(["customers"], "readwrite")
				transaction2.oncomplete = (event) ->
					console.log("#{ts()}: All data inserted in custumers! (fromIndex: #{fromIndex.min})")
				transaction2.onerror = (event) ->
					console.error("#{ts()}: Bad day for transactions (#{fromIndex.min}) #{event.message}")
					shouldContinue = false
			catch e
				console.error("#{ts()}: Settimg up transaction exception: #{e.message}")
				shouldContinue = false;
				
			console.log("#{ts()}: Getting a transacted objectStore (fromIndex: #{fromIndex.min})")
			objectStoreT = transaction2.objectStore("customers");
			
			for i in [fromIndex.min .. fromIndex.max]
				if shouldContinue 
					cd = { ssn: "444-44-4444_#{i}", name: "Bill", age: 35, email: "bill#{i}@company.com" }
					console.log("#{ts()}:  >>> #{i} #{cd.email}") if i%10000 == 0 || i < 3
					objectStoreT.add(cd)
				else
					console.warn("#{ts()}: #{i} database insertion interrupted (fromIndex: #{fromIndex.min})")
					
			console.warn("End of insertion for #{fromIndex.min}")
				
			
			
		## Starting point
		try
			console.log("#{ts()}: About to open the database")
			request = window.indexedDB.open("A8506231-215E-406A-B12D-EEEE97D7573G", 14)
		catch e
			console.error("#{ts()}: EXCEPTION raised while opening database#{e.message}")
		
		request.onerror = (event) ->
			console.error("#{ts()}: Error while opening the database: #{event.message}")
			
		request.onsuccess = (event) ->
			console.log("#{ts()}: Database successfully opened")
			shouldContinue = true
			db = request.result
			printDBInformation(db)
			if (shouldAddInitialData)
				console.log("Adding data...N= #{(sizeLimit / pageSize)}")
				for i2 in [0..(sizeLimit / pageSize)]
					if shouldContinue
						console.log("#{ts()}: Calling for #{i2} in #{(1000 + i2 * 2500)} ms")
						p = {
								min:i2*pageSize,
								max:i2*pageSize+pageSize
							}
						myStack.push(p)
						setTimeout ( -> 
							addData()
						), (1000 + i2 * 2500)
					else
						console.warn("#{ts()}: Database insertion interrupted at #{i2}")
						break
		
				console.log("Insertion done!")
			else
				console.warn("#{ts()}: shouldAddInitialData is false")
				
			printDBInformation(db)
		
		request.onupgradeneeded = (event) ->
			console.warn("Database schema needs to be upgraded/migrated *")

			console.log("Assigning db")
			db = request.result
			printDBInformation(db)
			
			#test if I have an existing objectStore, if so, drop it.
			if db.objectStoreNames.contains("customers")
				console.log("Deleting existing store")
				db.deleteObjectStore("customers")
			
			
			console.log("#{ts()}: createObjectStore")
			objectStore = db.createObjectStore("customers", { keyPath: "ssn" })
			
			console.log("#{ts()}: create indexes name + email")
			objectStore.createIndex("name", "name", { unique: false })
			objectStore.createIndex("email", "email", { unique: true })
			
			#signal when the database is fully opened
			#(request.onsucess) to add initial data
			shouldAddInitialData = true
			
			console.warn("#{ts()}: Database schema update completed.")

			
	
	else
		console.warn("#{ts()}: indexedDB is not supported")
		alert("Votre fureteur ne supporte pas la technologie indexedDB")