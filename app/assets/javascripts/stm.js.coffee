# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
    console.log("stm.js loaded")
    
    #Data URLs
    dataPath            = '/gtfs_stm/'
    agency_url          = dataPath + 'agency.txt'
    calendar_dates_url  = dataPath + 'calendar_dates.txt'
    fare_attributes_url = dataPath + 'fare_attributes.txt'
    fare_rules_url      = dataPath + 'fare_rules.txt'
    frequencies_url     = dataPath + 'frequencies.txt'
    routes_url          = dataPath + 'routes.txt'
    shapes_url          = dataPath + 'shapes.txt'
    stop_times_url      = dataPath + 'stop_times.txt'
    stops_url           = dataPath + 'stops.txt'
    trips_url           = dataPath + 'trips.txt'
    
    #dataset variables
    agency          = null
    calendar_dates  = null
    routes          = null
    
    #svg related suff
    container_dimensions = {width:900, height:400}
    margins = {top:50, right:20, bottom:30, left:60}
    chart_dimensions = {
        width: container_dimensions.width - margins.left - margins.right
        height: container_dimensions.height - margins.top - margins.bottom
    }
    

    #data loaders
    d3.csv agency_url,
        (d) -> return d
        (error, rows) ->
            agency = rows
            completeTask()
            
                
    d3.csv calendar_dates_url,
        (d) -> return d
        (error,rows) ->
            calendar_dates = rows
            completeTask()
            
    d3.csv routes_url,
        (d) -> return d
        (errors, rows) ->
            routes = rows
            routes.length = 4
            completeTask()
                
    #once a data loader complete, completeTask is called.
    #if every dataset are loaded (the nnn != null), completeTaks
    #perform the actual drawing
    completeTask = () ->
        if calendar_dates != null && agency != null && routes != null
            console.log("draw")
            chart = d3.select('#timeseries')
                        .append("svg")
                            .attr("width", container_dimensions.width)
                            .attr("height", container_dimensions.height)
                            .attr("style", "border: 1px solid black;")
                        .append("g")
                            .attr("transform",
                            "translate(" +
                                margins.left + "," +
                                margins.top+  ")")
                            .attr("id", "chart")
                            
            myText = d3.select('svg')
                        .append("text")
                            .text(agency[0].agency_name)
                            .attr("y", container_dimensions.height * 0.10)
                            .attr("x", chart_dimensions.width * 0.5)
                            .attr("id", 'myText')
                            .attr('style','text-anchor:middle;font-size: 18pt')
                            
            
            time_scale = d3.time.scale()
                .range([0,chart_dimensions.width])
                .domain([new Date(2008,0,1), new Date(2011,3,1)])
                
            percent_scale = d3.scale.linear()
                .range([chart_dimensions.height, 0])
                .domain([60,90])
                
            time_axis = d3.svg.axis()
                .scale(time_scale)
                .tickFormat( (date) ->
                    if (date.getMonth() == 0)
                        return moment(date).format('YYYY')
                    return moment(date).format("MMM"))
                
            count_axis = d3.svg.axis()
                .scale(percent_scale)
                .orient('left')
            
            x_axis_translate = "translate(0, #{chart_dimensions.height})"
            
            chart.append('g')
                .attr('class', 'x axis')
                .attr('transform', x_axis_translate)
                .call(time_axis)
                
            chart.append('g')
                .attr('class', 'y axis')
                .call(count_axis)
                
            d3.select('.y.axis')
                .append('text')
                    .attr("text-anchor", "middle")
                    .text("percent on time")
                    .attr("x", container_dimensions.height * 0.5)
                    .attr("y", 50)
                    .attr("transform", "rotate(-270,0,0)")
                    
            key_items = d3.select('#key')
                .selectAll('div')
                .data(routes)
                .enter()
                .append('div')
                    .attr('class', "key_line")
                    .attr('id', (d) ->
                        return d.route_id)
            
			key_items.append("div")
				.attr("id", (d) -> return "key_square" + d.route_id)
				.attr("style", (d) -> return "outline: solid 1px \#" + d.route_color)
				.attr("class", "key_square")

			key_items.append("div")
				.attr("class", "key_label")
				.attr("style", (d) -> return "color: \#" + d.route_color)
				.text((d) -> return d.route_long_name)


