# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

## The code under $-> is loaded once the whole html has finished rendering its content.
## it is equivalent of: $(document).ready(function() {}
$ ->
	$('body').addClass('welcome_js_loaded')
	console.log('welcome.js loaded *')
	