# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# -73.7485,45.4459,-73.715,45.4652


$ ->
	ts = () ->
		moment().format("ss.SSS")

	console.log("#{ts()} Projet carte topo")
	$('body').addClass('carte_topo')
	
	uniqueId = "A8506231-215E-406A-B12D-EEEE97D7573A"
	uniqueId2 = "A8506231-215E-406A-B12D-EEEE97D7573A_resultats"
	
	utmProjectionData = '+proj=utm +zone=18 +ellps=GRS80 +units=m +no_defs'
	utm = null
	utm_res = null
	
	console.log("#{ts()} utmProjectionData: #{utmProjectionData }")
	
	try
		
	catch e
		console.error("proj4 init returned error: #{e.message}")
	
	try
		mr = mgrs.toPoint("18TXR0000035000")
		console.log("mr #{mr}")
	catch e
		console.error("#{ts()} Error while getting mgrs conversion: #{e.message}")
		
	image_url = "/Dorval.png"
	imageBounds = L.latLngBounds([45.4459,-73.7485],[45.4652,-73.715])
							
	
	map = new L.Map("map", {
			center: [45.4555, -73.7311], 
			zoom: 14,
			minZoom:14,
			maxZoom:15,
			zoomControl:false})
				#.addLayer(new L.TileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png",
				#			{	attribution: "&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors " +
				#			"| &copy <a href=\"http://www.electionsquebec.qc.ca\">Le Directeur général des élections du Québec"}))
	#.fitBounds(imageBounds)
	try
		overlay = L.imageOverlay(image_url , imageBounds)
		    .addTo(map)
	catch e
		console.error("#{ts()} EXCEPTION while adding image to map: #{e.message}")
		
	map.dragging.disable();
	map.touchZoom.disable();
	map.doubleClickZoom.disable();
	map.scrollWheelZoom.disable();
	map.boxZoom.disable();
	map.keyboard.disable();
		
	console.log("#{ts()} Map creation completed, adding D3 layer")
	
	try
		#create the overlay d3 layer
		svg = d3.select(map.getPanes().overlayPane).append("svg")
		g = svg.append("g").attr("class", "leaflet-zoom-hide")
		
		
		svg.attr("width", "400")
			.attr("height", "329")
			.style("left", 0 + "px")
			.style("top", 0 + "px")
		
		
		
			
	catch e
		console.error("#{ts()} EXCEPTION while creating svg/g: #{e.message}")
		
	console.log("#{ts()} d3 layer created. About to load data")

	#utility functions
	projectPoint = (x, y) ->
		point = map.latLngToLayerPoint(new L.LatLng(y, x))
		this.stream.point(point.x, point.y)
	
	#setup the transformation mecanism
	transform = d3.geo.transform({point: projectPoint})
	path = d3.geo.path().projection(transform)
	
	mrs1 = mgrs.toPoint("18TXR0000035400")
	mrs2 = mgrs.toPoint("18TXR0000033250")
	
	top = 	45.4652
	bottom = 45.4459
	
	console.log("mrs1: #{mrs1}  mr2: #{mrs2}")
	
	# mr1: -73.7208959521441,45.461369717454204  mr2: -73.72150607600334,45.43437279952046
	
	#point1 = map.latLngToLayerPoint([mr1[1], mr1[0]])
	#point2 = map.latLngToLayerPoint([mr2[1], mr2[0]])
	
	pente = (mrs2[1] - mrs1[1]) / (mrs2[0] - mrs1[0])
	
	lngS = (bottom - mrs2[1]) / pente + mrs2[0]
	lngN = (top - mrs2[1]) / pente + mrs2[0]
	
	point1 = map.latLngToLayerPoint([top, lngN])
	point2 = map.latLngToLayerPoint([bottom, lngS])
	
	g.append("path")
		.attr("id", "mgrs_XR000")
		.attr("class", "mgrs strong")
		.attr("d", "M #{point1.x} #{point1.y} L #{point2.x} #{point2.y} ")
	
	# MGRS lines vertical (NS)
	for x in [977..1200] 
		if (x < 1000)
			pt1 = "18TWR#{x}0035400"
			pt2 = "18TWR#{x}0033250"
		else
			str_num = "a#{x}".substr(2,3)
			pt1 = "18TXR#{str_num}0035400"
			pt2 = "18TXR#{str_num}0033250"
			
		
		mrs1 = mgrs.toPoint(pt1)
		mrs2 = mgrs.toPoint(pt2)
		
		if (mrs2[0] != mrs1[0])
			pente = (mrs2[1] - mrs1[1]) / (mrs2[0] - mrs1[0])
			lngS = (bottom - mrs2[1]) / pente + mrs2[0]
			lngN = (top - mrs2[1]) / pente + mrs2[0]
			point1 = map.latLngToLayerPoint([top, lngN])
			point2 = map.latLngToLayerPoint([bottom, lngS])
			if (x % 10 != 0)
				classe = "mgrs verticl"
			else
				classe = "mgrs vertical strong"
			g.append("path")
				.attr("id", "vertical_mgrs_wr_#{x}")
				.attr("class", classe)
				.attr("d", "M #{point1.x} #{point1.y} L #{point2.x} #{point2.y} ")
			
	for y in [319..355]
		pt1 = "18TWR98000#{y}00"
		pt2 = "18TXR02000#{y}00"
		
		mrs1 = mgrs.toPoint(pt1)
		mrs2 = mgrs.toPoint(pt2)
		
		pente = (mrs2[1] - mrs1[1]) / (mrs2[0] - mrs1[0])
		
		left =  -73.7485
		right = -73.7150
		
		latE = mrs1[1] - pente * (left-mrs1[0])
		latW = mrs1[1] - pente * (right-mrs1[0])
		
		point1 = map.latLngToLayerPoint([latE, right])
		point2 = map.latLngToLayerPoint([latW, left])
		
		if (y % 10 != 0)
			classe = "mgrs horizontal"
		else
			classe = "mgrs horizontal strong"
		
		g.append("path")
			.attr("id", "horizonta_mgrs_wxr_#{x}")
			.attr("class", classe)
			.attr("d", "M #{point1.x} #{point1.y} L #{point2.x} #{point2.y} ")
		
		
	
	