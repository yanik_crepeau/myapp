# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->

	#Global variables
	svg = null		
	path = null		
	map	= null
	feature = null
	cercles= null
	uniqueId = "A8506231-215E-406A-B12D-EEEE97D7573A"
	uniqueId2 = "A8506231-215E-406A-B12D-EEEE97D7573A_resultats"

	#utility functions
	projectPoint = (x, y) ->
		point = map.latLngToLayerPoint(new L.LatLng(y, x))
		this.stream.point(point.x, point.y)

	ts = () ->
		moment().format("ss.SSS")
		
	#Create the background map.
	map = new L.Map("map", {center: [45.52, -73.6], zoom: 11})
		.addLayer(new L.TileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png",
					{	attribution: "&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors " +
					"| &copy <a href=\"http://www.electionsquebec.qc.ca\">Le Directeur général des élections du Québec"}))
	
	console.log("#{ts()} Leaflet map created.")

	#create the overlay d3 layer
	svg = d3.select(map.getPanes().overlayPane).append("svg")
	g = svg.append("g").attr("class", "leaflet-zoom-hide")
		
	console.log("#{ts()} d3 layer created. About to load data")
	
	#Get the overlay data used to draw constituencies lines

	drawConstituencies = (data) ->	
		console.log("#{ts()} data loaded, building the districts")
		
		##We work only with this portion of the file.
		geojsondata = topojson.feature(data, data.objects.electionsQC)
		
		## The reset() function redraw the map and update the path of every
		## district area. It is called when zooming in and out.
		reset = () ->
			##Reset the corners of the svg 
			bounds = path.bounds(geojsondata)
			topLeft = bounds[0]
			bottomRight = bounds[1]
			svg.attr("width", bottomRight[0] - topLeft[0])
				.attr("height", bottomRight[1] - topLeft[1])
				.style("left", topLeft[0] + "px")
				.style("top", topLeft[1] + "px")
			g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")")
			
			## Redraw the distrist borders using a new zoomed projection
			feature.attr("d", path)
			
			if (cercles != null)
				cercles
					.attr("transform", (d) ->
						centroid = map.latLngToLayerPoint(d.latLng)
						"translate(#{centroid.x}, #{centroid.y})")
					.attr("visibility", (d) ->
						rep = "novalue"
						if path.area(d)<500.0 
							rep = "hidden"
						else
							rep = "inherit"
							
						#console.log("#{d.properties.name} area: #{path.area(d)}  ––  #{rep}")
						rep)
					.attr("district_area", (d) ->
						path.area(d))
		
		
		
		#setup the transformation mecanism
		transform = d3.geo.transform({point: projectPoint})
		path = d3.geo.path().projection(transform)
		
		# Add the district to the map
		feature = g.selectAll(".district")
			.data(geojsondata.features)
			.enter()
			.append("path")
				.attr("id", (d) ->
					d.id)
				.attr("class", (d) ->
					"district d" + d.id)
				.attr("name", (d) ->
					d.properties.name + " QC")
					
		# And the dot
		console.log("#{ts()} pinning with 2nd...")
		cercles = g.selectAll("circle.pin")
			.data(geojsondata.features)
			.enter()
			.append("circle")
				.attr("id", (d) ->
					"c" + d.id)
				.attr("name", (d) ->
					d.properties.name)
				.attr("district_area", "nil")
				.attr("class", "pin")
				.attr("transform", (d) ->
					centroid = path.centroid(d)
					d.latLng = map.layerPointToLatLng(centroid )
					"translate(#{centroid[0]}, #{centroid[1]})")
				.attr("r", "5")
		
				
		#make sure when the map is zoomed, the reset is called, and
		#call reset anayway to complete the district drawing process		
		map.on("viewreset", reset);
		reset();
		

		console.log("#{ts()} Constituencies drawing completed!")




	
	displayResults = (elections2014) ->
		circonscriptions = elections2014.circonscriptions
		console.log("#{ts()} circonscriptions loaded (#{circonscriptions.length})")
		for circonscription, i in circonscriptions 
			numero = circonscription.numeroCirconscription
			candidats = circonscription.candidats
			partiElu = candidats[0].numeroPartiPolitique
			partiSecond = candidats[1].numeroPartiPolitique
			classeParti = ""
			
			if partiElu > 0
				switch partiElu
					when 6 then classeParti = "plq"
					when 8 then classeParti = "pq"
					when 11 then classeParti = "qs"
					when 27 then classeParti = "caq"
					else classeParti = "autreParti"
				repere = "path\##{numero}"
				#console.log("Assigning #{classeParti} to #{repere} - #{circonscription.nomCirconscription}")
				$(repere)[0]
					.setAttribute("class", "district d#{numero} elu #{classeParti}")

			if partiSecond > 0
				switch partiSecond 
					when 6 then classeParti = "plq"
					when 8 then classeParti = "pq"
					when 10 then classeParti = "vert"
					when 11 then classeParti = "qs"
					when 26 then classeParti = "on"
					when 27 then classeParti = "caq"
					else classeParti = "autreParti"
				repere = "circle\#c#{numero}"
				#console.log("Assigning pin #{classeParti} to #{repere} - #{circonscription.nomCirconscription}")
				$(repere)[0]
					.setAttribute("class", "pin d#{numero} second #{classeParti}")
		console.log("#{ts()} all items coloured accordint results")
	

		

	
	
	#local storage handlers (in project)
	load =  () ->
		#console.log("#{ts()}   >>> 1. call localStorage with id #{uniqueId}")
		local_data = localStorage[uniqueId]
		if (local_data != undefined && local_data != null)
			#console.log("#{ts()}   >>> 2. make it jason")
			rep = JSON.parse(local_data );
		else
			#console.log("#{ts()}   >>> 2. Oups... it's empty")
			rep = null;
		rep
	
 
	save = (obj) ->
		console.log("#{ts()} Saving data")	
		try
			localStorage[uniqueId] = JSON.stringify(obj);
			console.log("#{ts()} Saved data")
		catch error
			console.log("#{ts()} Error while saving data: #{error}")
	
	
	loadResults = () ->
		#console.log("#{ts()}   >>> 1. call localStorage with id #{uniqueId}")
		local_data = localStorage[uniqueId2]
		if (local_data != undefined && local_data != null)
			#console.log("#{ts()}   >>> 2. make it jason")
			rep = JSON.parse(local_data );
		else
			#console.log("#{ts()}   >>> 2. Oups... it's empty")
			rep = null;
		rep
		
	saveResults = (obj) ->
		console.log("#{ts()} Saving results")
		try 	
			localStorage[uniqueId2] = JSON.stringify(obj);
			console.log("#{ts()} saved results")
		catch error
			console.log("#{ts()} Error while saving results: #{error}")
		console.log("#{ts()} Saved results")
		
		
	
	
	#start here
	
	console.log("#{ts()} starting")
	data = load()
	
	if (data != undefined && data != null) 
		console.log("#{ts()} data found in local storage")
		drawConstituencies(data)
	else
		console.log("#{ts()} data NOT found in local storage")
		d3.json "/ca.json", (error, data) ->
			if (error)
				console.log("#{ts()} error loading data #{error}")
			else
				#save(data)
				drawConstituencies(data)
				
	
	data2 = loadResults()
	
	if (data2 != undefined && data2 != null)
		console.log("#{ts()} results found in local storage")
		displayResults(data2)
	else
		#Load the last election result data
		#and set the area color (using CSS)
		console.log("#{ts()} loading results from server ")
		d3.json "/resultats.json", (error, elections2014) ->
			if error 
				console.log("Unable to load /resultats.json #{error}")
				return
			else
				console.log("#{ts()} /resultats.json finished loading results")
				#saveResults(elections2014)
				displayResults(elections2014)
				
	console.log("#{ts()} that's all folks!")
				

		
	
