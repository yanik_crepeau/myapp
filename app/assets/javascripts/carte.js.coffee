# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
    console.log('map loading starts')
    #set the basic size of my map
    width = 1920
    height = 1080

    #append a svg element under the existing div element
    #with id carte_container. Set the width and height
    try
        svg = d3.select("#carte_container").append("svg")
            .attr("width", width)
            .attr("height", height)
    catch e
        console.log "ERROR: 17\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"


    #Open the source (json) document and load it

    d3.json "ca.json", (error,ca) ->
        if (error)
            #if the file 'ca.json' is not available or
            #can not be loaded, print an error and stop
            #here.
            console.log("Error while loading the file")
            console.error(error)
        else

            console.log("File received OK")

            #From this point, the ca object contains all my data
            try
                subunits     = topojson.feature(ca, ca.objects.subunits)
            catch e
                console.log "ERROR:37\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
                return

            #Pick a map projection, set scale and center.
            try
                projection     = d3.geo.mercator()
                    .scale(1500)
                    .translate([width * 1.20, height*2.15])
            catch e
                console.log "ERROR:46\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
                return


            #Create a path element according to the projection
            try
                path = d3.geo.path()
                    .projection(projection)
            catch e
                console.log "ERROR:55\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
                return


            #read the subunits elements in the json document
            #and create the back map.
            console.log("Notice: datum subunits")
            try
                svg.append("path")
                    .datum(subunits)
                    .attr("class", "back")
                    .attr("d", path)
            catch e
                console.log "ERROR:67\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"


            #Add the country shapes
            console.log("Notice: countries shapes")
            try
                svg.selectAll(".country")
                    .data(topojson.feature(ca, ca.objects.countries).features)
                    .enter()
                    .append("path")
                        .attr("class", (d) ->
                            "country " + d.id)
                        .attr("d", path)
            catch e
                console.log "ERROR:80\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
                return

            otherCountries = ["USA", "MEX", "CUB", "BLZ", "BHS"]

            #Setup boundries
            console.log("Notice: countries border")
            try
                svg.append("path")
                    .datum(topojson.mesh(ca, ca.objects.countries, (a, b) ->
                        a != b && a.id not in otherCountries))
                    .attr("d", path)
                    .attr("class", "subunit-boundary");
            catch e
                console.log "ERROR:93\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
                return

            #but for the USA, just draw an outline
            console.log("Notice: US outline")
            try
                svg.append("path")
                    .datum(topojson.mesh(ca, ca.objects.countries, (a, b) ->
                        a == b && a.id in otherCountries))
                    .attr("d", path)
                    .attr("class", "subunit-boundary USA");
            catch e
                console.log "ERROR:104\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
                return



            #Now, switch to another dataset, from elections Canada. Please,
            #when preparing data with ogr2ogr, make sure to force a conversion
            #to standard lat/long (-t_srs EPSG:4326).
            console.log("Notice: district")
            try
                svg.selectAll(".district")
                .data(topojson.feature(ca, ca.objects.electionsCA).features)
                .enter()
                .append("path")
                .attr("class", (d) -> "district d" + d.id)
                .attr("name", (d) -> d.properties.name + " " + d.properties.codeprov)
                .attr("d", path)
            catch e
                console.log "ERROR:123\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
                return


            console.log("End of data loading **** ")
