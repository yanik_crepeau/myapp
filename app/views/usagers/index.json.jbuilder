json.array!(@usagers) do |usager|
  json.extract! usager, :id, :nom, :courriel, :passe
  json.url usager_url(usager, format: :json)
end
